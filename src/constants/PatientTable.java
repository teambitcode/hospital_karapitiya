/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dilupa
 */
public class PatientTable {

        public static final String CLINIC_ID = "CLINIC_ID";
    public static final String ID_NUMBER = "ID_NUMBER";
    public static final String NAME = "NAME";
    public static final String ADDRESS = "ADDRESS";
    public static final String CONTACT_NUMBER = "CONTACT_NUMBER";
    public static final String WEIGHT = "WEIGHT";
    public static final String DIAGNOSIS = "DIAGNOSIS";
    public static final String GENDER = "GENDER";
    public static final String IS_NOTED = "IS_NOTED";
    
    public static String getReadableClinicId() {
        return "Clinic ID";
    }

    public static String getReadableNic() {
        return "NIC Number";
    }

    public static String getReadableName() {
        return "Name";
    }

    public static String getReadableWeight() {
        return "Weight";
    }

    public static String getReadableContactNUmber() {
        return "Contact Number";
    }

    public static String getReadableGender() {
        return "Gender";
    }
    public static String getReadableAddress() {
        return "Address";
    }

    public static final Map<String, String> getReadableNamesMap() {
        HashMap<String, String> patientTableReadableColunmNames = new HashMap<>();
        patientTableReadableColunmNames.put(CLINIC_ID, "Clinic ID");
        patientTableReadableColunmNames.put(ID_NUMBER, "ID Number");
        patientTableReadableColunmNames.put(NAME, "Name");
        patientTableReadableColunmNames.put(ADDRESS, "Address");
        patientTableReadableColunmNames.put(CONTACT_NUMBER, "Contact Number");
        patientTableReadableColunmNames.put(WEIGHT, "Weight");
        patientTableReadableColunmNames.put(GENDER, "Gender");
        patientTableReadableColunmNames.put(IS_NOTED, "Is Noted");
        patientTableReadableColunmNames.put(DIAGNOSIS, "Diagnosis");
                return patientTableReadableColunmNames;
    }
}
