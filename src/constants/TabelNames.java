/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

/**
 *
 * @author dilupa
 */
public final class TabelNames {

    public static final String PATIENT = "PATIENT";
    public static final String USERS = "USERS";
    public static final String DRUGS = "DRUGS";
    public static final String DRUG_RESERVATION = "DRUG_RESERVATION";
    public static final String PATIENT_CURRENT_TREATMENT_STORE = "PATIENT_CURRENT_TREATMENT_STORE";
    public static final String DRUG_RESERVATION_PER_PATIENT = "DRUG_RESERVATION_PER_PATIENT";

}
