/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dilupa
 */
public class DrugsTable {
    public static final String DRUG_ID = "DRUG_ID";
    public static final String DRUG_NAME = "DRUG_NAME";
    public static final String VIAL_COUNT = "VIAL_COUNT";
    public static final String CREATED_ON = "CREATED_ON";
    public static final String UPDATED_ON = "UPDATED_ON";
    
    
    public static final Map<String, String> getReadableNamesMap() {
        HashMap<String, String> drugsTableReadableColunmNames = new HashMap<>();
        drugsTableReadableColunmNames.put(DRUG_ID, "Drug ID");
        drugsTableReadableColunmNames.put(DRUG_NAME, "Drug Name");
        drugsTableReadableColunmNames.put(VIAL_COUNT, "Vial Count");
        drugsTableReadableColunmNames.put(CREATED_ON, "Created On");
        drugsTableReadableColunmNames.put(UPDATED_ON, "Updated On");
        return drugsTableReadableColunmNames;
    }
}
