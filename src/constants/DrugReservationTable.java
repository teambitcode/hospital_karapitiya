/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dilupa
 */
public class DrugReservationTable {
    public static final String DRUG_ID = "DRUG_ID";
    public static final String PATIENT_ID = "PATIENT_ID";
    public static final String MAB_NUMBER = "MAB_NUMBER";
    public static final String GROUP_NUMBER = "GROUP_NUMBER";
    public static final String STATUS = "STATUS";
    public static final String CREATED_ON = "CREATED_ON_DATE";
    
    
    public static final Map<String, String> getReadableNamesMap() {
        HashMap<String, String> drugsTableReadableColunmNames = new HashMap<>();
        drugsTableReadableColunmNames.put(DRUG_ID, "Drug ID");
        drugsTableReadableColunmNames.put(MAB_NUMBER, "MAB Number");
        drugsTableReadableColunmNames.put(GROUP_NUMBER, "Group Number");
        drugsTableReadableColunmNames.put(STATUS, "Status");
        drugsTableReadableColunmNames.put(PATIENT_ID, "PATIENT_ID");
        drugsTableReadableColunmNames.put(CREATED_ON, "Date");
        return drugsTableReadableColunmNames;
    }
    public static final Map<String, String> getFieldNamesMap() {
        HashMap<String, String> drugsTableReadableColunmNames = new HashMap<>();
        drugsTableReadableColunmNames.put(DRUG_ID, "DRUG_ID");
        drugsTableReadableColunmNames.put(MAB_NUMBER, "MAB_NUMBER");
        drugsTableReadableColunmNames.put(GROUP_NUMBER, "GROUP_NUMBER");
        drugsTableReadableColunmNames.put(STATUS, "STATUS");
        drugsTableReadableColunmNames.put(CREATED_ON, "CREATED_ON_DATE");
        drugsTableReadableColunmNames.put(PATIENT_ID, "PATIENT_ID");
        return drugsTableReadableColunmNames;
    }
}
