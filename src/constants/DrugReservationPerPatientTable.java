/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

/**
 *
 * @author dilupa
 */
public class DrugReservationPerPatientTable {
     public static final String ID = "ID";
    public static final String CLINIC_ID = "CLINIC_ID";
    public static final String DRUG_ID = "DRUG_ID";
    public static final String VIAL_COUNT = "VIAL_COUNT";
    public static final String UPDATED_DATE = "UPDATED_DATE";
}
