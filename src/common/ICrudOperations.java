/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.sql.ResultSet;

/**
 *
 * @author dilupa
 */
public interface ICrudOperations {
    public ResultSet getAll();
    public Boolean insert();
    public Boolean update();
    public Boolean deleteOne();
}
