/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import com.toedter.calendar.JDateChooser;
import constants.DrugReservationTable;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.swing.JLabel;

/**
 *
 * @author dilupa
 */
public class CommonUtils {

    public static String generateSqlForInsert(String tableName, Map<String, Object> insertData) {
        String sql = "INSERT INTO " + tableName + " (";
        String valueMarks = "VALUES(";
        for (Map.Entry<String, Object> entry : insertData.entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue());
            sql += entry.getKey() + ",";
            valueMarks += "?,";
        }
        sql = sql.substring(0, sql.length() - 1) + ") ";
        valueMarks = valueMarks.substring(0, valueMarks.length() - 1) + ")";
        return sql + valueMarks;
    }

    public static String generateSqlForBatchInsert(String tableName) {

        String values = "";
        HashMap<String, String> drugReservationFields = new HashMap<>();
        drugReservationFields = (HashMap<String, String>) DrugReservationTable.getFieldNamesMap();

        for (int i = 0; i < drugReservationFields.size(); i++) {
            values += "?,";
        }
        values = values.substring(0, values.length() - 1);
        return "INSERT INTO " + tableName + "  VALUES (" + values + ",?)";

    }
    public static String generateSqlForBatchUpdate(String tableName) {

        String values = "";
        HashMap<String, String> drugReservationFields = new HashMap<>();
        drugReservationFields = (HashMap<String, String>) DrugReservationTable.getFieldNamesMap();

        values += drugReservationFields.get("DRUG_ID") + "=?,";
        values += drugReservationFields.get("MAB_NUMBER") + "=?,";
        values += drugReservationFields.get("GROUP_NUMBER") + "=?,";
        values += drugReservationFields.get("STATUS") + "=?,";
        values += drugReservationFields.get("CREATED_ON_DATE") + "=?,";
        values += drugReservationFields.get("PATIENT_ID") + "=?,";

        values = values.substring(0, values.length() - 1);
        return "update " + tableName + " set " + values + " where DRUG_RESERVATION_ID=?";

    }
    
        public static String generateSqlForUpdate(String tableName,Map<String, Object> updateData, String idColoumn, Object id) {

        String value = "";
        HashMap<String, String> drugReservationFields = new HashMap<>();
        drugReservationFields = (HashMap<String, String>) DrugReservationTable.getFieldNamesMap();

            for (Map.Entry<String, Object> entry : updateData.entrySet()) {
                System.out.println(entry.getKey() + "/" + entry.getValue());
                if (entry.getValue().getClass().equals(String.class)) {
//                    pst.setString(index, entry.getValue().toString());
                    value += entry.getKey().toString() + "='" + entry.getValue().toString() + "',";
                }
                System.out.println(value);
                if (entry.getValue().getClass().equals(Integer.class)) {
                    value += value + entry.getKey().toString() + "=" + entry.getValue() + ",";
                }

            }
            value = value.substring(0, value.length() - 1);

            return "update " + tableName + " set " + value + " where " + idColoumn + "='" + id + "'";

    }
    public static String generateSqlForDelete(String tableName, String conditionField) {

        return "delete from " + tableName +" where "+conditionField+"=?";

    }
    public static String generateDetailFromMap(Map<String, String> insertData) {
        String details = "";
        for (Map.Entry<String, String> entry : insertData.entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue());
            details += " " + entry.getKey() + "  " + " : " + entry.getValue() + "\n";
        }
        return details;
    }
    
    public static String getSelectedFieldsFromMap(Set<String> selectedFieldSet) {
        Iterator<String> iterator = selectedFieldSet.iterator();
        String slectedColumns = "";
        while (iterator.hasNext()) {
            slectedColumns += iterator.next() + ",";
        }
        slectedColumns = slectedColumns.substring(0, slectedColumns.length() - 1);
        return slectedColumns;
    }
    public static HashMap<String, String> inspect(Class className) {
        Field[] fields = className.getDeclaredFields();
        System.out.printf("%d fields:%n", fields.length);
              HashMap<String, String> tableFieldsMap = new HashMap<>();
        for (Field field : fields) {
               tableFieldsMap.put(field.getName(), field.getName());
        }
        return tableFieldsMap;
    }
    
    public static String escapeStringForMySQL(String s) {
        return s.replaceAll("\\", "\\\\")
                .replaceAll("\b","\\b")
                .replaceAll("\n","\\n")
                .replaceAll("\r", "\\r")
                .replaceAll("\t", "\\t")
                .replaceAll("\\x1A", "\\Z")
                .replaceAll("\\x00", "\\0")
                .replaceAll("'", "\\'")
                .replaceAll("\"", "\\\"");
    }

    public static String escapeWildcardsForMySQL(String s) {
        return escapeStringForMySQL(s)
                .replaceAll("%", "\\%")
                .replaceAll("_","\\_");
    }
    public static Boolean checkValidStringText(String stringValue) {
        boolean status = true;
        if (stringValue.toString() == null
                || stringValue.toString().isEmpty()) {
            status = false;
        }
        return status;
    }
    public static Boolean checkValidOptinalStringText(String stringValue) {
        boolean status = true;
        if (stringValue.toString() == null
                || stringValue.toString().isEmpty()) {
            status = false;
        }
        return status;
    }
    
    public static Boolean checkIsNumberTypeString(String number) {
        String regex = "\\d+";
        try {
            return number.matches(regex);
        } catch (Exception e) {
            return false;
        }
    }
    public static Boolean checkValidDropDownItem(int index) {
        boolean status = true;
        if (index != 0) {
            status = true;
        } else {
            status = false;
        }
        return status;
    }
    public static Boolean checkValidStringNumberWithZero(String intValue) {
        boolean status = false;
        if (checkIsNumberTypeString(intValue)) {
            if (!intValue.toString().isEmpty()) {
                status = true;
            } else {
                status = false;
            }
        }

        return status;
    }
    public static Boolean checkValidStringNumber(String intValue) {
        boolean status = false;
        if (checkIsNumberTypeString(intValue)) {
            if (!intValue.toString().isEmpty() && Integer.parseInt(intValue.toString()) != 0) {
                status = true;
            } else {
                status = false;
            }
        }

        return status;
    }
    public static Boolean checkValidOptinalStringNumber(String intValue) {
        boolean status = false;
        if (intValue != null && intValue != "") {
            if (checkIsNumberTypeString(intValue)) {
                if (!intValue.toString().isEmpty() && Integer.parseInt(intValue.toString()) != 0) {
                    status = true;
                } else {
                    status = false;
                }
            }
        } else {
            status = true;
        }
        return status;
    }
   
    public static Boolean checkValidStringWithLength(int maxLength, String text, JLabel label, String errorMessage) {
        boolean status = true;
        if (text.length() > maxLength-1) {
            label.setText(errorMessage);
            label.setVisible(true);
            status = false;
        }else{
        label.setVisible(false);
        status = true;
        }
        return status;
    }
    public static Boolean checkValidNumberWithLength(int maxLength, String intValue, JLabel label, String errorMessage) {
        boolean status = true;
        if(checkIsNumberTypeString(intValue)){
        if (intValue.length() > maxLength) {
            label.setText(errorMessage);
            label.setVisible(true);
            status = false;
        }else{
        label.setVisible(false);
        status = true;
        }
        }
        
        return status;
    }
    public static Boolean compairTwoDates(Date startTypeDate, Date endTypeDate) {
        boolean status = true;
        int r = endTypeDate.compareTo(startTypeDate);
        if(endTypeDate.compareTo(startTypeDate)> 0){
        status = true;
        }else{
            status = false;
        }
        return status;
    }
    public static void resetDateChooserToCurrentDate(JDateChooser jDateChooser) {

        DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy");
        Date date = new Date();
        jDateChooser.setDate(date);
    }
    public static Boolean checkDate(Date startTypeDate) {
        if (startTypeDate == null) {
            return false;
        }else {
        return true;
        }
    }
}
