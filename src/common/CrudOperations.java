/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import constants.DrugsTable;
import constants.PatientTable;
import constants.TabelNames;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import models.DrugReservation;

/**
 *
 * @author dilupa
 */
public class CrudOperations {
public static ResultSet SelectAll(Connection connection, String tableName,Set<String> selectedFieldSet) {
        ResultSet rst = null;
        
        
        String slectedColumns = CommonUtils.getSelectedFieldsFromMap(selectedFieldSet);
       

        String sql = "SELECT "+slectedColumns+" FROM " + tableName;
        try {
            PreparedStatement pst = (PreparedStatement) connection.prepareStatement(sql);
            rst = pst.executeQuery();
//                user_table.setModel(DbUtils.resultSetToTableModel(rst));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return rst;
    }

public static Boolean deleteItem(Connection connection, String tableName, Object id,String idName, String customQuary) {
        boolean rst = false;
        String sql = "";
        if(customQuary != null){
        sql = customQuary;
        }else{
            sql = "DELETE FROM " + tableName + " WHERE " + idName + " = " + id + ";";
        }
        try {
            PreparedStatement pst = (PreparedStatement) connection.prepareStatement(sql);
            rst = pst.execute();
//                user_table.setModel(DbUtils.resultSetToTableModel(rst));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return rst;
    }

public static Boolean update(Connection connection, String tableName, Object id, Map<String, Object> updateData, String idColomn) {
        boolean rst = true;
    String sql = CommonUtils.generateSqlForUpdate(tableName, updateData, idColomn, id);

    try {
        PreparedStatement pst = (PreparedStatement) connection.prepareStatement(sql);
        rst = pst.execute();
        System.out.println(rst);
    } catch (SQLException e) {
        System.out.println(e);
        return false;
    }
    return !rst;
    }   


public static ResultSet SelectAll(Connection connection, String tableName) {
        ResultSet rst = null;
        String sql = "SELECT * FROM " + tableName;
        try {
            PreparedStatement pst = (PreparedStatement) connection.prepareStatement(sql);
            rst = pst.executeQuery();
//                user_table.setModel(DbUtils.resultSetToTableModel(rst));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return rst;
    }
public static ResultSet getItemsDetailsByCondition(Connection connection, String tableName,String conditionColoumn, String conditionValue) {
        ResultSet rst = null;
        String sql = "SELECT * FROM " + tableName + " WHERE LOWER("+conditionColoumn+") LIKE LOWER('%"+conditionValue+"%')";
        try {
            PreparedStatement pst = (PreparedStatement) connection.prepareStatement(sql);
            rst = pst.executeQuery();
//                user_table.setModel(DbUtils.resultSetToTableModel(rst));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return rst;
    }
    public static ResultSet getItemDetailsById(Connection connection, String tableName,String idField, String id) {
        ResultSet rst = null;
        String sql = "SELECT * FROM " + tableName + " WHERE "+idField+"="+"'"+id+"'";
        try {
            PreparedStatement pst = (PreparedStatement) connection.prepareStatement(sql);
            rst = pst.executeQuery();
//                user_table.setModel(DbUtils.resultSetToTableModel(rst));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return rst;
    }
//    public static Boolean InsertBatch(Connection connection, ArrayList<DrugReservation> drugReservationList,String tableName) {
//        String insertSql = "INSERT INTO student VALUES (?, ?, ?)";
//        PreparedStatement pstmt = con.prepareStatement(insertSql);
//        for (Student student : students) {
//            pstmt.setString(1, student.getId()); //not sure if String or int or long
//            pstmt.setString(2, student.getName());
//            pstmt.setString(3, student.getCity());
//            pstmt.addBatch();
//        }
//        pstmt.executeBatch();
//    }    
    public static Boolean executeCustomQuaryFromExecute(Connection connection,String quary) {
        boolean execute = false;
        ResultSet rst = null;
        try {
            PreparedStatement pst = (PreparedStatement) connection.prepareStatement(quary);
          execute = pst.execute();
//                user_table.setModel(DbUtils.resultSetToTableModel(rst));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return execute;
    
    }    
    public static ResultSet executeCustomQuary(Connection connection,String quary) {
    ResultSet rst = null;
        try {
            PreparedStatement pst = (PreparedStatement) connection.prepareStatement(quary);
            rst = pst.executeQuery();
//                user_table.setModel(DbUtils.resultSetToTableModel(rst));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return rst;
    }
    public static Boolean InsertOne(Connection connection, Map<String, Object> insertData,String tableName) {
        ResultSet rst = null;
        boolean execute = false;
        String sql = CommonUtils.generateSqlForInsert(tableName,insertData);
        try {
            int index = 1;
            PreparedStatement pst = (PreparedStatement) connection.prepareStatement(sql);
            for (Map.Entry<String, Object> entry : insertData.entrySet()) {
                System.out.println(entry.getKey() + "/" + entry.getValue());
                if(entry.getValue().getClass().equals(String.class)){
                   pst.setString(index, entry.getValue().toString());
                }
                if(entry.getValue().getClass().equals(Integer.class)){
                   pst.setInt(index, Integer.parseInt(entry.getValue().toString()));
                }
                index += 1;
            }
            execute = pst.execute();
        } catch (Exception e) {
            System.out.print(e);
            return null;
        }
        return execute;
    }

    public static HashMap<String, Object> getMapFromResultSet(ResultSet resultSet, String tableName) throws SQLException {
        HashMap<String, Object> tableFieldsMap = new HashMap<>();
        while (resultSet.next()) {

            switch (tableName) {
                case TabelNames.PATIENT:
                    String CLINIC_ID = resultSet.getString(PatientTable.CLINIC_ID);
                    tableFieldsMap.put(PatientTable.CLINIC_ID, CLINIC_ID);
                    String NAME = resultSet.getString(PatientTable.NAME);
                    tableFieldsMap.put(PatientTable.NAME, NAME);
                    String ADDRESS = resultSet.getString(PatientTable.ADDRESS);
                    tableFieldsMap.put(PatientTable.ADDRESS, ADDRESS);
                    String CONTACT_NUMBER = resultSet.getString(PatientTable.CONTACT_NUMBER);
                    tableFieldsMap.put(PatientTable.CONTACT_NUMBER, CONTACT_NUMBER);
                    String DIAGNOSIS = resultSet.getString(PatientTable.DIAGNOSIS);
                    tableFieldsMap.put(PatientTable.DIAGNOSIS, DIAGNOSIS);
                    String GENDER = resultSet.getString(PatientTable.GENDER);
                    tableFieldsMap.put(PatientTable.GENDER, GENDER);
                    String ID_NUMBER = resultSet.getString(PatientTable.ID_NUMBER);
                    tableFieldsMap.put(PatientTable.ID_NUMBER, ID_NUMBER);
                    String IS_NOTED = resultSet.getString(PatientTable.IS_NOTED);
                    tableFieldsMap.put(PatientTable.IS_NOTED, IS_NOTED);
                    String WEIGHT = resultSet.getString(PatientTable.WEIGHT);
                    tableFieldsMap.put(PatientTable.WEIGHT, WEIGHT);

                    //code to be executed;    
                    break;  //optional  
                case TabelNames.DRUGS:
                    String DRUG_ID = resultSet.getString(DrugsTable.DRUG_ID);
                    tableFieldsMap.put(DrugsTable.DRUG_ID, DRUG_ID);
                    String DRUG_NAME = resultSet.getString(DrugsTable.DRUG_NAME);
                    tableFieldsMap.put(DrugsTable.DRUG_NAME, DRUG_NAME);
                    String CREATED_ON = resultSet.getString(DrugsTable.CREATED_ON);
                    tableFieldsMap.put(DrugsTable.CREATED_ON, CREATED_ON);
                    String UPDATED_ON = resultSet.getString(DrugsTable.UPDATED_ON);
                    tableFieldsMap.put(DrugsTable.UPDATED_ON, UPDATED_ON);
                    String VIAL_COUNT = resultSet.getString(DrugsTable.VIAL_COUNT);
                    tableFieldsMap.put(DrugsTable.VIAL_COUNT, VIAL_COUNT);
                    //code to be executed;    
                    break;  //optional 
                case TabelNames.USERS:
                    break;
            }
        }

        return tableFieldsMap;
    }
}
