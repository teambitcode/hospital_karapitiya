/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.interfaces;

import common.CommonIcons;
import common.CommonUtils;
import common.CrudOperations;
import constants.DrugReservationPerPatientTable;
import constants.DrugsTable;
import constants.TabelNames;
import database.DbConnection;
import java.awt.Color;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import models.Drug;
import models.DrugReservationPerPatientData;
import models.Patient;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author dilupa
 */
public class DrugReservationPerPatient extends javax.swing.JFrame {

    Connection conn = null;
    ArrayList<DrugReservationPerPatientData> drugReservationPerPatientList = new ArrayList<>();
    ArrayList<Drug> drugList = new ArrayList<>();
    CommonIcons imageIconObject = new CommonIcons();
    int current_index = 0;
    int current_id = 0;
    String errorMessage = "";
    String clinic_id = null;
    int crrent_drug_id=0;
    /**
     * Creates new form MedicineThings
     */
    public DrugReservationPerPatient() throws SQLException {
        initComponents();
        DbConnection dbConnection = DbConnection.getInstance();
        conn = dbConnection.getConnection();
        btn_update.setVisible(false);
        btn_delete.setVisible(false);
         btn_cancel.setVisible(false);
        btn_add.setVisible(true);
        lbl_ok_patient_data_load_by_clinic_id.setVisible(false);
        lbl_error_patient_data_load_by_clinic_id.setVisible(false);
//        loadData();
        loadCustomTable();
        getAllDrugs();
    }
    public DrugReservationPerPatient(String id) throws SQLException {
        initComponents();
        DbConnection dbConnection = DbConnection.getInstance();
        conn = dbConnection.getConnection();
        btn_update.setVisible(false);
        btn_delete.setVisible(false);
         btn_cancel.setVisible(false);
        btn_add.setVisible(true);
        setClinic_id(id);
        loadDataByConditon();
        loadCustomTable();
        getAllDrugs();
    }

    public String getClinic_id() {
        return clinic_id;
    }

    public void setClinic_id(String clinic_id) {
        this.clinic_id = clinic_id;
    }
    
 public void loadData(){
        try {
            String sql = "SELECT *, drp.VIAL_COUNT as P_VIAL_COUNT FROM DRUGS d,DRUG_RESERVATION_PER_PATIENT drp WHERE d.DRUG_ID = drp.DRUG_ID";
            ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);
//            drugTable.setModel(DbUtils.resultSetToTableModel(rst));
                drugReservationPerPatientList.clear();
            while (rst.next()) {
                DrugReservationPerPatientData drugReservationPerPatientData = new DrugReservationPerPatientData();
                drugReservationPerPatientData.setId(rst.getInt("ID"));
                drugReservationPerPatientData.setDrug_id(rst.getInt("DRUG_ID"));
                drugReservationPerPatientData.setDrug_name(rst.getString("DRUG_NAME"));
                String h =rst.getString("VIAL_COUNT");
                drugReservationPerPatientData.setVial_count(Integer.parseInt(rst.getString("P_VIAL_COUNT")) );
                drugReservationPerPatientData.setDate(rst.getString("UPDATED_DATE"));
//                                drug.setUpdatedOn(rst.getString("UPDATED_ON"));
                this.drugReservationPerPatientList.add(drugReservationPerPatientData);
            }
                
        } catch (Exception e) {
                System.out.println(e);
        }
    }

    private Integer getDropDownDrugName(String name) {
        int drugId = 0;
        String[] values = name.split("-");
        if (!values[0].contains("Item")) {
            List<Drug> selectedDrug = drugList
                    .stream()
                    .filter(c -> c.getDrugId().equals(values[0]))
                    .collect(Collectors.toList());
            return selectedDrug.size() > 0 ? Integer.parseInt(selectedDrug.get(0).getDrugId()) : 0;
        }
        return 0;
    }
 public LocalDateTime getDateAsLocalDateTime(Date date) {
        LocalDateTime ldt = null;
        if (CommonUtils.checkDate(date)) {
            ldt = LocalDateTime.ofInstant(date.toInstant(),
                    ZoneId.systemDefault());
        }
        return ldt;
    }
 public void getAllDrugs() {
        Set<String> setA = new HashSet<>();


        setA.add(DrugsTable.DRUG_NAME);
        setA.add(DrugsTable.DRUG_ID);
        setA.add(DrugsTable.CREATED_ON);
        setA.add(DrugsTable.VIAL_COUNT);
        setA.add(DrugsTable.UPDATED_ON);

        ResultSet drugResultSet = CrudOperations.SelectAll(conn, TabelNames.DRUGS, setA);
        try {
            drpd_drugList.removeAllItems();
            drugList.clear();
            while (drugResultSet.next()) {
                Drug drugItem = new Drug();
                drugItem.setDrugId(drugResultSet.getString(DrugsTable.DRUG_ID));
                drugItem.setDrugName(drugResultSet.getString(DrugsTable.DRUG_NAME));
                drugItem.setVialCount(drugResultSet.getInt(DrugsTable.VIAL_COUNT));
                drugItem.setCreatedOn(drugResultSet.getString(DrugsTable.CREATED_ON));
                drugItem.setUpdatedOn(drugResultSet.getString(DrugsTable.UPDATED_ON));
                this.drugList.add(drugItem);
                drpd_drugList.addItem(drugResultSet.getString(DrugsTable.DRUG_ID)+"-" + drugResultSet.getString(DrugsTable.DRUG_NAME));
            }
//            if(this.drugReservationPerPatientList.size() > 0){
//                setDrugStockLabel(jLabel1, this.drugReservationPerPatientList.get(0).getVialCount());
//                Drug.checkDrugAvailability(this.drugReservationPerPatientList.get(0).getDrugId(), this.drugReservationPerPatientList, jLabel1);
//            }
//            this.drpd_drugList.setModel((Drug<String>) drugReservationPerPatientList);
        } catch (Exception e) {
        }
    }
    public void resetFrom() {
        drpd_drugList.setSelectedIndex(0);
        txt_number_of_vials.setText("");
    }
 public void loadDataByConditon(){
// reservationList
     clinic_id = txt_drug_search.getText().toString();
        boolean isEmpty = true;
        try {
            if (txt_drug_search.getText() != null && txt_drug_search.getText().toString() != null && !txt_drug_search.getText().toString().isEmpty()) {
//                ResultSet rst = Drug.getPatientsByCondition(conn, DrugReservationPerPatientTable.CLINIC_ID, txt_drug_search.getText().toString());
                String sql = "SELECT *, drp.VIAL_COUNT as P_VIAL_COUNT FROM DRUGS d,DRUG_RESERVATION_PER_PATIENT drp WHERE drp.CLINIC_ID = '" + clinic_id + "' and d.DRUG_ID = drp.DRUG_ID";
                ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);
                drugReservationPerPatientList.clear();
                while (rst.next()) {
                    isEmpty = false;
                    DrugReservationPerPatientData drugReservationPerPatientData = new DrugReservationPerPatientData();
                    drugReservationPerPatientData.setId(rst.getInt("ID"));
                    drugReservationPerPatientData.setClinic_id(rst.getString("CLINIC_ID"));
                    drugReservationPerPatientData.setDrug_id(rst.getInt("DRUG_ID"));
                    drugReservationPerPatientData.setDrug_name(rst.getString("DRUG_NAME"));
                    drugReservationPerPatientData.setVial_count(rst.getInt("P_VIAL_COUNT"));
                    drugReservationPerPatientData.setDate(rst.getString("UPDATED_DATE"));
                    this.drugReservationPerPatientList.add(drugReservationPerPatientData);
                    loadCustomTable();
                }
                if (isEmpty) {
                    loadCustomTable();
//                    JOptionPane.showMessageDialog(null, "No result found", "Error", JOptionPane.ERROR_MESSAGE);
                }
                                    setPatientDetails();
            } else {
                clinic_id = null;
                JOptionPane.showMessageDialog(null, "Please enter search condition", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } catch (Exception e) {
        }
    }
 
    public void setPatientDetails() {
        clinic_id = null;
        boolean isEmpty = true;
        try {
            ResultSet rst = Patient.getOnePatientById(conn, txt_drug_search.getText().toString());
            while (rst.next()) {
                isEmpty = false;
                clinic_id = rst.getString("CLINIC_ID");
                lbl_patient_name.setText(rst.getString("NAME"));
                lbl_patient_clinic_number.setText(rst.getString("CLINIC_ID"));
                lbl_patient_id.setText(rst.getString("ID_NUMBER"));
            }
            if (isEmpty) {
                clinic_id = null;
                JOptionPane.showMessageDialog(null, "No Records found", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            clinic_id = null;
            JOptionPane.showMessageDialog(null, "Somthing went wrong", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
 public void loadCustomTable() {
        drugTable.setModel(new DefaultTableModel(new Object[][]{},
                new String[]{
                    "ID", "Drug Name", "Vial Count"
                }));
        DefaultTableModel model = (DefaultTableModel) drugTable.getModel();

        for (int i = 0; i < drugReservationPerPatientList.size(); i++) {

            model.addRow(new Object[]{
                drugReservationPerPatientList.get(i).geId(),
                drugReservationPerPatientList.get(i).getDrug_id()+"-"+drugReservationPerPatientList.get(i).getDrug_name(),
                drugReservationPerPatientList.get(i).getVial_count()
            });
        }
    }

    public Boolean valid() {
        boolean status = true;
        if (!CommonUtils.checkValidStringNumberWithZero(txt_number_of_vials.getText())) {
            status = false;
            errorMessage = "Please enter valid vial count";
        }
        if (clinic_id == null || clinic_id.isEmpty()) {
            status = false;
            errorMessage = "Please enter valid Clinic ID";
        }
        if (status) {
            errorMessage = "";
        }
        return status;
    }
    public void loadTableDrugDropDownList(){
        try {
            ResultSet rst = Drug.getAllDrugsAsResultSetForTable(conn);
//            drpdwn_drug.removeAllItems();
            while (rst.next()) {
                Drug drug = new Drug();
                drug.setDrugId(Integer.toString(rst.getInt("DRUG_ID")));
                drug.setDrugName(rst.getString("DRUG_NAME"));
                drug.setVialCount(rst.getInt("VIAL_COUNT"));
//                                drug.setCreatedOn(rst.getString("CREATED_ON"));
//                                drug.setUpdatedOn(rst.getString("UPDATED_ON"));
//                this.drugReservationPerPatientList.add(drug);
//                drpdwn_drug.addItem(drug.getDrugId() + " - " + drug.getDrugName());
            }
                
        } catch (Exception e) {
                System.out.println(e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        drpd_drugList = new javax.swing.JComboBox<>();
        lbl_ok_patient_data_load_by_clinic_id = new javax.swing.JLabel();
        lbl_error_patient_data_load_by_clinic_id = new javax.swing.JLabel();
        lbl_patient_name = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lbl_patient_clinic_number = new javax.swing.JLabel();
        lbl_patient_id = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txt_drug_search = new javax.swing.JTextField();
        jButton6 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btn_cancel = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        btn_delete = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        lbl_add_val = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        drugTable = new javax.swing.JTable();
        btn_add = new javax.swing.JButton();
        btn_update = new javax.swing.JButton();
        txt_number_of_vials = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1000, 700));
        setPreferredSize(new java.awt.Dimension(1000, 700));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(51, 0, 51));
        jPanel1.setToolTipText("");
        jPanel1.setLayout(null);

        drpd_drugList.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        drpd_drugList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                drpd_drugListActionPerformed(evt);
            }
        });
        jPanel1.add(drpd_drugList);
        drpd_drugList.setBounds(220, 310, 230, 28);

        lbl_ok_patient_data_load_by_clinic_id.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/save.png"))); // NOI18N
        jPanel1.add(lbl_ok_patient_data_load_by_clinic_id);
        lbl_ok_patient_data_load_by_clinic_id.setBounds(560, 190, 40, 40);

        lbl_error_patient_data_load_by_clinic_id.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/warning.png"))); // NOI18N
        jPanel1.add(lbl_error_patient_data_load_by_clinic_id);
        lbl_error_patient_data_load_by_clinic_id.setBounds(560, 190, 40, 40);

        lbl_patient_name.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_patient_name.setForeground(new java.awt.Color(255, 255, 255));
        lbl_patient_name.setText("--------------");
        jPanel1.add(lbl_patient_name);
        lbl_patient_name.setBounds(240, 210, 180, 20);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Name");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(120, 210, 70, 17);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Clinic Number");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(120, 240, 100, 17);

        lbl_patient_clinic_number.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_patient_clinic_number.setForeground(new java.awt.Color(255, 255, 255));
        lbl_patient_clinic_number.setText("--------------");
        jPanel1.add(lbl_patient_clinic_number);
        lbl_patient_clinic_number.setBounds(240, 240, 180, 17);

        lbl_patient_id.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_patient_id.setForeground(new java.awt.Color(255, 255, 255));
        lbl_patient_id.setText("--------------");
        jPanel1.add(lbl_patient_id);
        lbl_patient_id.setBounds(240, 270, 190, 17);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("NIC Number");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(120, 270, 100, 17);

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Treatment");
        jPanel1.add(jLabel17);
        jLabel17.setBounds(130, 320, 80, 17);

        txt_drug_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_drug_searchActionPerformed(evt);
            }
        });
        jPanel1.add(txt_drug_search);
        txt_drug_search.setBounds(320, 140, 300, 28);

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/search_label.png"))); // NOI18N
        jButton6.setText("Search");
        jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton6MouseClicked(evt);
            }
        });
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton6);
        jButton6.setBounds(680, 130, 110, 40);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Clinic NUmber");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(180, 130, 110, 40);

        btn_cancel.setBackground(new java.awt.Color(204, 204, 204));
        btn_cancel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        jPanel1.add(btn_cancel);
        btn_cancel.setBounds(760, 410, 100, 40);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Treatment Store");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(340, 30, 310, 50);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/people.png"))); // NOI18N
        jButton2.setText("All Patients");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(60, 40, 150, 40);

        btn_delete.setBackground(new java.awt.Color(255, 168, 168));
        btn_delete.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_delete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/removed.png"))); // NOI18N
        btn_delete.setText("Delete");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });
        jPanel1.add(btn_delete);
        btn_delete.setBounds(630, 410, 110, 40);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/home.png"))); // NOI18N
        jButton3.setText("Home");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(770, 40, 150, 40);

        lbl_add_val.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        jPanel1.add(lbl_add_val);
        lbl_add_val.setBounds(120, 110, 740, 70);

        drugTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        drugTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                drugTableMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                drugTableMouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(drugTable);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(130, 470, 740, 130);

        btn_add.setBackground(new java.awt.Color(204, 255, 204));
        btn_add.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/add.png"))); // NOI18N
        btn_add.setText("Add");
        btn_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addActionPerformed(evt);
            }
        });
        jPanel1.add(btn_add);
        btn_add.setBounds(480, 410, 100, 40);

        btn_update.setBackground(new java.awt.Color(255, 253, 203));
        btn_update.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_update.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/update.png"))); // NOI18N
        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });
        jPanel1.add(btn_update);
        btn_update.setBounds(490, 410, 120, 40);

        txt_number_of_vials.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_number_of_vialsActionPerformed(evt);
            }
        });
        jPanel1.add(txt_number_of_vials);
        txt_number_of_vials.setBounds(710, 310, 120, 28);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Number of vials");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(580, 320, 120, 17);

        jLabel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 10, 960, 630);

        jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));
        jPanel1.add(jLabel5);
        jLabel5.setBounds(60, 94, 860, 520);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/walpapers/home.jpg"))); // NOI18N
        jLabel3.setText("jLabel1");
        jLabel3.setMaximumSize(new java.awt.Dimension(900, 600));
        jLabel3.setMinimumSize(new java.awt.Dimension(900, 600));
        jLabel3.setPreferredSize(new java.awt.Dimension(900, 600));
        jPanel1.add(jLabel3);
        jLabel3.setBounds(0, 0, 1000, 800);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1000, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1000, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 800, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 800, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_drug_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_drug_searchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_drug_searchActionPerformed

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
        // TODO add your handling code here:
        DrugReservationPerPatientData newDrug = new DrugReservationPerPatientData();

        Boolean deletePatient = newDrug.deleteDrugById(conn, current_id);
        if (deletePatient == null) {
            JOptionPane.showMessageDialog(null, "This drug already used. Cannot remove this drug.", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            if (deletePatient == false) {
                JOptionPane.showMessageDialog(null, "Successfully removed", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
                loadData();
                loadCustomTable();
                btn_cancel.setVisible(false);
                btn_add.setVisible(true);
                txt_drug_search.setEnabled(true);
                resetFrom();
                
            } else {
                JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
                loadData();
                loadCustomTable();
            }
            btn_update.setVisible(false);
            btn_delete.setVisible(false);
        }
        
    }//GEN-LAST:event_btn_deleteActionPerformed

    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
        // TODO add your handling code here:
        String drug = this.drpd_drugList.getSelectedItem().toString();
        String[] values = drug.split("-");
        LocalDateTime now = LocalDateTime.now();
        if (valid()) {
            
            DrugReservationPerPatientData newDrugReservationPerPatientData = new DrugReservationPerPatientData();
            newDrugReservationPerPatientData.setClinic_id(clinic_id);
            newDrugReservationPerPatientData.setDate(now.toString());
            newDrugReservationPerPatientData.setDrug_id(Integer.parseInt(values[0]));
            newDrugReservationPerPatientData.setVial_count(Integer.parseInt(this.txt_number_of_vials.getText().toString()));

            Boolean insertPatient = newDrugReservationPerPatientData.insertDrugReservationPerPatient(conn);
            if (!insertPatient) {
                JOptionPane.showMessageDialog(null, "Successfully added", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
                loadDataByConditon();
                loadCustomTable();
                resetFrom();
            } else {
                JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
                loadData();
                loadCustomTable();
            }
        } else {
            JOptionPane.showMessageDialog(null,errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
        }

        
    }//GEN-LAST:event_btn_addActionPerformed

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
        // TODO add your handling code here:
        if(valid()){
        boolean status = false;
        Drug drug = new Drug();
        HashMap<String, Object> drugReservationPerPatientData = new HashMap<>();
//        drugData.put(DrugsTable.DRUG_ID, current_id);
        drugReservationPerPatientData.put(DrugReservationPerPatientTable.CLINIC_ID, clinic_id);
            drugReservationPerPatientData.put(DrugReservationPerPatientTable.DRUG_ID, crrent_drug_id);
                drugReservationPerPatientData.put(DrugReservationPerPatientTable.UPDATED_DATE, new Date());
        drugReservationPerPatientData.put(DrugReservationPerPatientTable.VIAL_COUNT, Integer.parseInt(this.txt_number_of_vials.getText().toString()));
               
        status = DrugReservationPerPatientData.updateDrug(conn, drugReservationPerPatientData, current_id);
        if (status) {
            JOptionPane.showMessageDialog(null, "Successfully updated", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
            loadData();
            loadCustomTable();
            loadTableDrugDropDownList();
            btn_cancel.setVisible(false);
            btn_add.setVisible(true);
            btn_update.setVisible(false);
            btn_delete.setVisible(false);
            txt_drug_search.setEnabled(true);
            resetFrom();
        } else {

            JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
        }
        }else{
        JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_btn_updateActionPerformed

    private void txt_number_of_vialsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_number_of_vialsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_number_of_vialsActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
      Home home;
        try {
            home = new Home();
            home.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(MedicalTreatments.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
      AllPatients allPatients;
        try {
            allPatients = new AllPatients();
            allPatients.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void drugTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drugTableMouseClicked
        // TODO add your handling code here:
        txt_drug_search.setEnabled(false);
//        txt_drug_search.setBackground(Color.GRAY);
        btn_cancel.setVisible(true);
        btn_add.setVisible(false);
        btn_update.setVisible(true);
        btn_delete.setVisible(true);
        int row = drugTable.getSelectedRow();
        current_index = row + 1;
        txt_number_of_vials.setText(drugTable.getValueAt(row, 2).toString());
        current_id = Integer.parseInt(drugTable.getValueAt(row, 0).toString());
        String drug = drugTable.getValueAt(row, 1).toString();
        crrent_drug_id = getDropDownDrugName(drug);
        drpd_drugList.setSelectedItem(drug);
    }//GEN-LAST:event_drugTableMouseClicked

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        // TODO add your handling code here:
        btn_cancel.setVisible(false);
        btn_delete.setVisible(false);
        btn_update.setVisible(false);
        btn_add.setVisible(true);
        current_id = -1;
        txt_number_of_vials.setText("");
        txt_drug_search.setText("");
        txt_drug_search.setEnabled(true);
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void jButton6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseClicked
        // TODO add your handling code here:
        //        432423CLINICID123
          if (txt_drug_search.getText() != null && (!txt_drug_search.getText().toString().equals("")) && (!txt_drug_search.getText().toString().isEmpty())) {
            loadDataByConditon();
        } else {
            loadData();
            loadCustomTable();
        }
    }//GEN-LAST:event_jButton6MouseClicked

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        if (txt_drug_search.getText() != null && (!txt_drug_search.getText().toString().equals("")) && (!txt_drug_search.getText().toString().isEmpty())) {
            loadDataByConditon();
        } else {
            loadCustomTable();
        }

    }//GEN-LAST:event_jButton6ActionPerformed

    private void drpd_drugListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_drpd_drugListActionPerformed
        // TODO add your handling code here:
//        JComboBox comboBox = (JComboBox) evt.getSource();
//        if(comboBox.getSelectedItem() != null){
//            String selected = comboBox.getSelectedItem().toString();
//            String[] values = selected.split("-");
//            if (!values[0].contains("Item")) {
//                List<Drug> selectedDrug = drugReservationPerPatientList
//                .stream()
//                .filter(c -> c.getDrugId().equals(values[0]))
//                .collect(Collectors.toList());
//                setDrugStockLabel(jLabel1, selectedDrug.get(0).getVialCount());
//                Drug.checkDrugAvailability(selectedDrug.get(0).getDrugId(), this.drugReservationPerPatientList, jLabel1);
//            }
//        }

    }//GEN-LAST:event_drpd_drugListActionPerformed

    private void drugTableMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drugTableMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_drugTableMouseEntered

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DrugReservationPerPatient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DrugReservationPerPatient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DrugReservationPerPatient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DrugReservationPerPatient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new DrugReservationPerPatient().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(DrugReservationPerPatient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add;
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_update;
    private javax.swing.JComboBox<String> drpd_drugList;
    private javax.swing.JTable drugTable;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_add_val;
    private javax.swing.JLabel lbl_error_patient_data_load_by_clinic_id;
    private javax.swing.JLabel lbl_ok_patient_data_load_by_clinic_id;
    private javax.swing.JLabel lbl_patient_clinic_number;
    private javax.swing.JLabel lbl_patient_id;
    private javax.swing.JLabel lbl_patient_name;
    private javax.swing.JTextField txt_drug_search;
    private javax.swing.JTextField txt_number_of_vials;
    // End of variables declaration//GEN-END:variables
}
