/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.interfaces;

import common.CommonIcons;
import common.CommonUtils;
import common.CrudOperations;
import constants.PatientTable;
import constants.TabelNames;
import database.DbConnection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import models.Patient;

/**
 *
 * @author dilupa
 */
public class PatientsRegistration extends javax.swing.JFrame {

    Connection conn = null;
    String errorMessage = "";
    String clinicId = "";
    CommonIcons imageIconObject = new CommonIcons();
    /**
     * Creates new form PatientsRegistration
     */
    public PatientsRegistration() throws SQLException {
        initComponents();
        DbConnection dbConnection = DbConnection.getInstance();
        conn = dbConnection.getConnection();
        btnPatientRegister.setVisible(true);
        btnPatientUpdate.setVisible(false);
        btnPatientRemove.setVisible(false);
        lbl_ok_patient_data_load_by_clinic_id.setVisible(false);
        lbl_error_patient_data_load_by_clinic_id.setVisible(false);
        btn_clear_update.setVisible(false);
        btn_clear_register.setVisible(true);
        resetErrorLabels();
    }

    public PatientsRegistration(String clinicId, boolean isUpdate) throws SQLException {
        initComponents();
        DbConnection dbConnection = DbConnection.getInstance();
        conn = dbConnection.getConnection();
        btnPatientRegister.setVisible(false);
        btnPatientUpdate.setVisible(true);
        btnPatientRemove.setVisible(true);
        txtPatientClinicNumber.setEnabled(false);
        lbl_ok_patient_data_load_by_clinic_id.setVisible(false);
        lbl_error_patient_data_load_by_clinic_id.setVisible(false);
        btn_clear_update.setVisible(true);
        btn_clear_register.setVisible(false);
        resetErrorLabels();
    }
    public void resetErrorLabels(){

        lbl_name_error.setVisible(false);
        lbl_address_error.setVisible(false);
        lbl_clinic_error.setVisible(false);
        lbl_nic_error.setVisible(false);
        lbl_contact_error.setVisible(false);
        lbl_error_detail.setVisible(false);
        lbl_weight_error.setVisible(false);
        lbl_gender_error.setVisible(false);

    }
    public void setPatientClinicId(String clinicId) {
        this.clinicId = clinicId;
    }
     public void loadCurrentPatientData(Boolean showResponse) {
        try {
            ResultSet rst = Patient.getOnePatientById(conn, clinicId);
            HashMap<String, Object> tableFieldsMap = new HashMap<>();

            tableFieldsMap = CrudOperations.getMapFromResultSet(rst, TabelNames.PATIENT);
            if (tableFieldsMap.size() > 0) {
                txtPatientName.setText(tableFieldsMap.get(PatientTable.NAME).toString());
                txtPatientAddress.setText(tableFieldsMap.get(PatientTable.ADDRESS).toString());
                txtPatientClinicNumber.setText(tableFieldsMap.get(PatientTable.CLINIC_ID).toString());
                txtPatientIdNumber.setText(tableFieldsMap.get(PatientTable.ID_NUMBER).toString());
                txtPatientContactNumber.setText(tableFieldsMap.get(PatientTable.CONTACT_NUMBER).toString());
                txtPatientWeight.setText(tableFieldsMap.get(PatientTable.WEIGHT).toString());
                cmbPatientGender.setSelectedItem(tableFieldsMap.get(PatientTable.GENDER).toString());
                txtPatientDiagnosis.setText(tableFieldsMap.get(PatientTable.DIAGNOSIS).toString());
                System.out.println(tableFieldsMap);

                showHideLables(lbl_ok_patient_data_load_by_clinic_id);
            } else {
                showHideLables(lbl_error_patient_data_load_by_clinic_id);
            }

        } catch (Exception e) {
            try {
                showHideLables(lbl_error_patient_data_load_by_clinic_id);
            } catch (InterruptedException ex) {
                Logger.getLogger(PatientsRegistration.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
        public void showHideLables(JLabel labelName) throws InterruptedException {
        labelName.setVisible(true);
//        Thread.currentThread().sleep(4000);
//        labelName.setVisible(false);
        int delay = 5000; //milliseconds
        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                labelName.setVisible(false);
            }
        };
        new javax.swing.Timer(delay, taskPerformer).start();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lbl_ok_patient_data_load_by_clinic_id = new javax.swing.JLabel();
        lbl_error_patient_data_load_by_clinic_id = new javax.swing.JLabel();
        lbl_address_error = new javax.swing.JLabel();
        lbl_error_detail = new javax.swing.JLabel();
        lbl_gender_error = new javax.swing.JLabel();
        lbl_weight_error = new javax.swing.JLabel();
        lbl_clinic_error = new javax.swing.JLabel();
        lbl_contact_error = new javax.swing.JLabel();
        lbl_nic_error = new javax.swing.JLabel();
        lbl_name_error = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        lbl_diagnosis_error1 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        btn_clear_register = new javax.swing.JButton();
        btn_clear_update = new javax.swing.JButton();
        btnPatientRemove = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtPatientAddress = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtPatientName = new javax.swing.JTextArea();
        txtPatientIdNumber = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtPatientClinicNumber = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        btnPatientUpdate = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtPatientDiagnosis = new javax.swing.JTextArea();
        txtPatientContactNumber = new javax.swing.JTextField();
        txtPatientWeight = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        btnPatientRegister = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cmbPatientGender = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(600, 400));
        setPreferredSize(new java.awt.Dimension(1000, 700));
        setResizable(false);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(51, 0, 51));
        jPanel1.setToolTipText("");
        jPanel1.setLayout(null);

        lbl_ok_patient_data_load_by_clinic_id.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/save.png"))); // NOI18N
        jPanel1.add(lbl_ok_patient_data_load_by_clinic_id);
        lbl_ok_patient_data_load_by_clinic_id.setBounds(670, 40, 40, 40);

        lbl_error_patient_data_load_by_clinic_id.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/warning.png"))); // NOI18N
        jPanel1.add(lbl_error_patient_data_load_by_clinic_id);
        lbl_error_patient_data_load_by_clinic_id.setBounds(670, 40, 40, 40);

        lbl_address_error.setFont(new java.awt.Font("Batang", 1, 11)); // NOI18N
        lbl_address_error.setForeground(new java.awt.Color(255, 0, 0));
        lbl_address_error.setText("Maximum length exceeded");
        jPanel1.add(lbl_address_error);
        lbl_address_error.setBounds(610, 210, 280, 14);

        lbl_error_detail.setFont(new java.awt.Font("Batang", 1, 11)); // NOI18N
        lbl_error_detail.setForeground(new java.awt.Color(255, 0, 0));
        lbl_error_detail.setText("Please enter valid Patient Diagnosis");
        jPanel1.add(lbl_error_detail);
        lbl_error_detail.setBounds(610, 480, 250, 20);

        lbl_gender_error.setFont(new java.awt.Font("Batang", 1, 11)); // NOI18N
        lbl_gender_error.setForeground(new java.awt.Color(255, 0, 0));
        lbl_gender_error.setText("Please select gender");
        jPanel1.add(lbl_gender_error);
        lbl_gender_error.setBounds(200, 400, 280, 14);

        lbl_weight_error.setFont(new java.awt.Font("Batang", 1, 11)); // NOI18N
        lbl_weight_error.setForeground(new java.awt.Color(255, 0, 0));
        lbl_weight_error.setText("Please enter valid Patient Weight");
        jPanel1.add(lbl_weight_error);
        lbl_weight_error.setBounds(610, 330, 280, 14);

        lbl_clinic_error.setFont(new java.awt.Font("Batang", 1, 11)); // NOI18N
        lbl_clinic_error.setForeground(new java.awt.Color(255, 0, 0));
        lbl_clinic_error.setText("Please enter valid Patient Clinic Number");
        jPanel1.add(lbl_clinic_error);
        lbl_clinic_error.setBounds(610, 270, 280, 14);

        lbl_contact_error.setFont(new java.awt.Font("Batang", 1, 11)); // NOI18N
        lbl_contact_error.setForeground(new java.awt.Color(255, 0, 0));
        lbl_contact_error.setText("Please enter valid Contact Number");
        jPanel1.add(lbl_contact_error);
        lbl_contact_error.setBounds(200, 330, 280, 14);

        lbl_nic_error.setFont(new java.awt.Font("Batang", 1, 11)); // NOI18N
        lbl_nic_error.setForeground(new java.awt.Color(255, 0, 0));
        lbl_nic_error.setText("Maximum length exceeded");
        jPanel1.add(lbl_nic_error);
        lbl_nic_error.setBounds(200, 270, 280, 14);

        lbl_name_error.setFont(new java.awt.Font("Batang", 1, 11)); // NOI18N
        lbl_name_error.setForeground(new java.awt.Color(255, 0, 0));
        lbl_name_error.setText("Please enter valid Patient Name");
        jPanel1.add(lbl_name_error);
        lbl_name_error.setBounds(200, 210, 280, 14);

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 0, 0));
        jLabel15.setText("*");
        jPanel1.add(jLabel15);
        jLabel15.setBounds(590, 250, 20, 17);

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 0, 0));
        jLabel16.setText("*");
        jPanel1.add(jLabel16);
        jLabel16.setBounds(860, 570, 20, 17);

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 0, 0));
        jLabel17.setText("*");
        jPanel1.add(jLabel17);
        jLabel17.setBounds(590, 310, 20, 17);

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 0, 0));
        jLabel18.setText("*");
        jPanel1.add(jLabel18);
        jLabel18.setBounds(590, 380, 20, 17);

        lbl_diagnosis_error1.setFont(new java.awt.Font("Batang", 1, 14)); // NOI18N
        lbl_diagnosis_error1.setForeground(new java.awt.Color(255, 255, 255));
        lbl_diagnosis_error1.setText("Required ");
        jPanel1.add(lbl_diagnosis_error1);
        lbl_diagnosis_error1.setBounds(780, 570, 80, 17);

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 0, 0));
        jLabel19.setText("*");
        jPanel1.add(jLabel19);
        jLabel19.setBounds(170, 380, 20, 17);

        btn_clear_register.setBackground(new java.awt.Color(204, 204, 204));
        btn_clear_register.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btn_clear_register.setText("Clear");
        btn_clear_register.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_clear_registerActionPerformed(evt);
            }
        });
        jPanel1.add(btn_clear_register);
        btn_clear_register.setBounds(480, 510, 160, 50);

        btn_clear_update.setBackground(new java.awt.Color(204, 204, 204));
        btn_clear_update.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btn_clear_update.setText("Clear");
        btn_clear_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_clear_updateActionPerformed(evt);
            }
        });
        jPanel1.add(btn_clear_update);
        btn_clear_update.setBounds(660, 510, 160, 50);

        btnPatientRemove.setBackground(new java.awt.Color(255, 168, 168));
        btnPatientRemove.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPatientRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/removed.png"))); // NOI18N
        btnPatientRemove.setText("Delete");
        btnPatientRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPatientRemoveActionPerformed(evt);
            }
        });
        jPanel1.add(btnPatientRemove);
        btnPatientRemove.setBounds(200, 500, 160, 50);

        txtPatientAddress.setColumns(5);
        txtPatientAddress.setLineWrap(true);
        txtPatientAddress.setRows(3);
        txtPatientAddress.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPatientAddressKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(txtPatientAddress);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(610, 140, 280, 70);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Address");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(530, 170, 56, 17);

        txtPatientName.setColumns(5);
        txtPatientName.setLineWrap(true);
        txtPatientName.setRows(3);
        txtPatientName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPatientNameKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(txtPatientName);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(200, 140, 280, 70);

        txtPatientIdNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPatientIdNumberActionPerformed(evt);
            }
        });
        txtPatientIdNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPatientIdNumberKeyPressed(evt);
            }
        });
        jPanel1.add(txtPatientIdNumber);
        txtPatientIdNumber.setBounds(200, 240, 230, 28);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("NIC Number");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(90, 250, 90, 17);

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 0, 0));
        jLabel14.setText("*");
        jPanel1.add(jLabel14);
        jLabel14.setBounds(180, 170, 20, 17);

        txtPatientClinicNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPatientClinicNumberActionPerformed(evt);
            }
        });
        txtPatientClinicNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPatientClinicNumberKeyPressed(evt);
            }
        });
        jPanel1.add(txtPatientClinicNumber);
        txtPatientClinicNumber.setBounds(610, 240, 230, 28);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Clinic Number");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(490, 250, 100, 17);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Gender");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(120, 380, 60, 17);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Register Patient");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(340, 30, 310, 50);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Diagnosis");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(520, 380, 70, 17);

        btnPatientUpdate.setBackground(new java.awt.Color(255, 253, 203));
        btnPatientUpdate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPatientUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/update.png"))); // NOI18N
        btnPatientUpdate.setText("Update");
        btnPatientUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPatientUpdateActionPerformed(evt);
            }
        });
        jPanel1.add(btnPatientUpdate);
        btnPatientUpdate.setBounds(480, 510, 160, 50);

        txtPatientDiagnosis.setColumns(5);
        txtPatientDiagnosis.setLineWrap(true);
        txtPatientDiagnosis.setRows(3);
        txtPatientDiagnosis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPatientDiagnosisKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(txtPatientDiagnosis);

        jPanel1.add(jScrollPane3);
        jScrollPane3.setBounds(610, 350, 280, 130);

        txtPatientContactNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPatientContactNumberActionPerformed(evt);
            }
        });
        txtPatientContactNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPatientContactNumberKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPatientContactNumberKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPatientContactNumberKeyTyped(evt);
            }
        });
        jPanel1.add(txtPatientContactNumber);
        txtPatientContactNumber.setBounds(200, 300, 230, 28);

        txtPatientWeight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPatientWeightActionPerformed(evt);
            }
        });
        txtPatientWeight.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPatientWeightKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPatientWeightKeyReleased(evt);
            }
        });
        jPanel1.add(txtPatientWeight);
        txtPatientWeight.setBounds(610, 300, 110, 28);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/people.png"))); // NOI18N
        jButton2.setText("All Patients");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(60, 40, 150, 40);

        btnPatientRegister.setBackground(new java.awt.Color(204, 255, 204));
        btnPatientRegister.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPatientRegister.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/add.png"))); // NOI18N
        btnPatientRegister.setText("Register ");
        btnPatientRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPatientRegisterActionPerformed(evt);
            }
        });
        jPanel1.add(btnPatientRegister);
        btnPatientRegister.setBounds(300, 510, 160, 50);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/home.png"))); // NOI18N
        jButton3.setText("Home");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(770, 40, 150, 40);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Full Name");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(114, 170, 70, 17);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Contact No.");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(90, 310, 90, 17);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Weight (Kg)");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(500, 310, 90, 17);

        cmbPatientGender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Gender", "Male", "Female", "Ven" }));
        cmbPatientGender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPatientGenderActionPerformed(evt);
            }
        });
        jPanel1.add(cmbPatientGender);
        cmbPatientGender.setBounds(200, 370, 150, 28);

        jLabel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 10, 960, 630);

        jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));
        jPanel1.add(jLabel5);
        jLabel5.setBounds(60, 94, 860, 500);

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/walpapers/home.jpg"))); // NOI18N
        jLabel12.setText("jLabel1");
        jLabel12.setMaximumSize(new java.awt.Dimension(900, 600));
        jLabel12.setMinimumSize(new java.awt.Dimension(900, 600));
        jLabel12.setPreferredSize(new java.awt.Dimension(900, 600));
        jPanel1.add(jLabel12);
        jLabel12.setBounds(0, 0, 1000, 800);

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Full Name");
        jPanel1.add(jLabel13);
        jLabel13.setBounds(114, 170, 70, 17);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1000, 800);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPatientIdNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPatientIdNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPatientIdNumberActionPerformed

    private void txtPatientClinicNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPatientClinicNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPatientClinicNumberActionPerformed

    private void txtPatientContactNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPatientContactNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPatientContactNumberActionPerformed

    private void txtPatientWeightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPatientWeightActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPatientWeightActionPerformed

    private void btnPatientRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPatientRegisterActionPerformed
        // TODO add your handling code here:
        if (checkIsValid()) {
            Patient newPatient = new Patient();
            newPatient.setClinicId(this.txtPatientClinicNumber.getText().toString());
            newPatient.setIdNumber(this.txtPatientIdNumber.getText().toString());
            newPatient.setName(this.txtPatientName.getText().toString());
            newPatient.setAddress(this.txtPatientAddress.getText().toString());
            newPatient.setContactNumber(this.txtPatientContactNumber.getText().toString());
            newPatient.setWeight(this.txtPatientWeight.getText().toString());
            newPatient.setDiagnosis(this.txtPatientDiagnosis.getText().toString());
            newPatient.setGender(this.cmbPatientGender.getSelectedItem().toString());
            newPatient.setIsNoted("false");

            Boolean insertPatient = newPatient.insertPatient(conn);
            if (insertPatient == null) {
                JOptionPane.showMessageDialog(null, "Clinic ID already exists", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                if (insertPatient == false) {
                    JOptionPane.showMessageDialog(null, "Successfully added", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
                    try {
//                        AllPatients allPatients = new AllPatients();
//                        allPatients.setVisible(true);
//                        this.dispose();
                    } catch (Exception e) {
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
                }

            }
        } else {
            JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
        }


    }//GEN-LAST:event_btnPatientRegisterActionPerformed
    public Boolean checkIsValid() {
        boolean status = true;
       
        if (!CommonUtils.checkValidStringWithLength(190, txtPatientName.getText().toString(), lbl_name_error, "Full name maximum length exceeded")) {
            errorMessage = "Please enter valid Patient Name";
            status = false;
        }
        if (!CommonUtils.checkValidStringText(txtPatientName.getText())) {
            errorMessage = "Please enter valid Patient Name";
            status = false;
            lbl_name_error.setVisible(true);
        }
        
        
        if (!CommonUtils.checkValidStringWithLength(190, txtPatientAddress.getText().toString(), lbl_address_error, "Address maximum length exceeded")) {
            errorMessage = "Please enter valid Patient Address";
            status = false;
        }

//        if (!CommonUtils.checkValidStringText(txtPatientIdNumber.getText())) {
        if (!CommonUtils.checkValidStringWithLength(12, txtPatientIdNumber.getText().toString(), lbl_nic_error, "NIC maximum length exceeded")) {
            errorMessage = "Please enter valid Patient NIC Number";
            status = false;
        }

        if (!CommonUtils.checkValidStringWithLength(49, txtPatientClinicNumber.getText().toString(), lbl_clinic_error, "Clinic number maximum length exceeded")) {
            errorMessage = "Please enter valid Patient Clinic Number";
            status = false;
        }
        if (!CommonUtils.checkValidStringText(txtPatientClinicNumber.getText())) {
            errorMessage = "Please enter valid Patient Clinic Number";
            status = false;
            lbl_clinic_error.setVisible(true);
        }

//        if (!CommonUtils.checkValidOptinalStringNumber(txtPatientContactNumber.getText())) {
//            errorMessage = "Please enter valid Patient Contact Number";
//            status = false;
//        }
        if (!CommonUtils.checkValidNumberWithLength(12, txtPatientContactNumber.getText().toString(), lbl_contact_error, "Contact number maximum length exceeded")) {
            errorMessage = "Please enter valid Contact Number";
            status = false;
            lbl_contact_error.setVisible(true);
        }
         if(txtPatientContactNumber.getText() != null && !txtPatientContactNumber.getText().toString().isEmpty()){
             if (!CommonUtils.checkValidStringNumber(txtPatientContactNumber.getText())) {
                 errorMessage = "Please enter valid Contact Number";
                 status = false;
                 lbl_contact_error.setVisible(true);
             }
         }

        if (!CommonUtils.checkValidNumberWithLength(5, txtPatientWeight.getText().toString(), lbl_weight_error, "Weight maximum length exceeded")) {
            errorMessage = "Please enter valid Patient Weight";
            status = false;
            lbl_weight_error.setVisible(true);
        }
        if (!CommonUtils.checkValidStringNumber(txtPatientWeight.getText())) {
            errorMessage = "Please enter valid Patient Weight";
            status = false;
            lbl_weight_error.setVisible(true);
        }

        if (!CommonUtils.checkValidDropDownItem(cmbPatientGender.getSelectedIndex())) {
            errorMessage = "Please enter valid Patient Gender";
            status = false;
            lbl_gender_error.setVisible(true);
        }
        
        if (!CommonUtils.checkValidStringWithLength(799, txtPatientDiagnosis.getText().toString(), lbl_error_detail, "Diagnosis maximum length exceeded")) {
            errorMessage = "Please enter valid Patient Diagnosis";
            status = false;
        }
        if (!CommonUtils.checkValidStringText(txtPatientDiagnosis.getText())) {
            errorMessage = "Please enter valid Patient Diagnosis";
            status = false;
            lbl_error_detail.setVisible(true);
        }
        return status;
    }
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        Home home;
        try {
            home = new Home();
            home.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(MedicalTreatments.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
       AllPatients allPatients;
        try {
            allPatients = new AllPatients();
            allPatients.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnPatientUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPatientUpdateActionPerformed
        // TODO add your handling code here:
        if (checkIsValid()) {
            Patient patient = new Patient();
            HashMap<String, Object> patientData = new HashMap<>();
            patientData.put(PatientTable.CLINIC_ID, this.txtPatientClinicNumber.getText().toString());
            patientData.put(PatientTable.NAME, this.txtPatientName.getText().toString());
            patientData.put(PatientTable.ID_NUMBER, this.txtPatientIdNumber.getText().toString());
            patientData.put(PatientTable.ADDRESS, this.txtPatientAddress.getText().toString());
            patientData.put(PatientTable.CONTACT_NUMBER, this.txtPatientContactNumber.getText().toString());
            patientData.put(PatientTable.DIAGNOSIS, this.txtPatientDiagnosis.getText().toString());
            patientData.put(PatientTable.GENDER, this.cmbPatientGender.getSelectedItem().toString());
            patientData.put(PatientTable.WEIGHT, this.txtPatientWeight.getText().toString());
//        patientData.put(PatientTable.IS_NOTED, );
            try {
                boolean status = true;
                status = patient.updatePatient(conn, patientData, clinicId);
                if (status) {
                    showHideLables(lbl_ok_patient_data_load_by_clinic_id);
                    JOptionPane.showMessageDialog(null, "Successfully updated", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
                    AllPatients allPatients = new AllPatients();
                    allPatients.setVisible(true);
                    this.dispose();
                } else {
                    showHideLables(lbl_error_patient_data_load_by_clinic_id);
                    JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception e) {
                try {
                    showHideLables(lbl_error_patient_data_load_by_clinic_id);
                    JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PatientsRegistration.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnPatientUpdateActionPerformed

    private void btnPatientRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPatientRemoveActionPerformed
        // TODO add your handling code here:
        int input = JOptionPane.showConfirmDialog(null, "Do you want to remove this patient?");
        // 0=yes, 1=no, 2=cancel
        System.out.println(input);
        if (input == 0) {
            boolean status = Patient.deletePatient(conn, TabelNames.PATIENT, "'" + clinicId + "'", PatientTable.CLINIC_ID);
            if (status) {
                JOptionPane.showMessageDialog(null, "Successfully deleted", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
                AllPatients allPatients;
                try {
                    allPatients = new AllPatients();
                    allPatients.setVisible(true);
                    this.dispose();
                } catch (SQLException ex) {
                    Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Cannot delete because this patient has previous treatment records. Only patients without previous records can be deleted", "Error", JOptionPane.ERROR_MESSAGE);

            }
        }

    }//GEN-LAST:event_btnPatientRemoveActionPerformed

    private void txtPatientNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPatientNameKeyPressed
        // TODO add your handling code here:
        if (txtPatientName.getText().toString().length() > 0) {
            CommonUtils.checkValidStringWithLength(190, txtPatientName.getText().toString(), lbl_name_error, "Full name maximum length exceeded");
        }
    }//GEN-LAST:event_txtPatientNameKeyPressed

    private void txtPatientWeightKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPatientWeightKeyPressed
        // TODO add your handling code here:
        CommonUtils.checkValidNumberWithLength(5, txtPatientWeight.getText().toString(), lbl_weight_error, "Invalid weight");

        if (CommonUtils.checkValidStringNumber(txtPatientWeight.getText())) {
            lbl_weight_error.setVisible(false);
        } else {
            lbl_weight_error.setVisible(true);
        }
    }//GEN-LAST:event_txtPatientWeightKeyPressed

    private void txtPatientAddressKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPatientAddressKeyPressed
        // TODO add your handling code here:
        CommonUtils.checkValidStringWithLength(190,txtPatientAddress.getText().toString(),lbl_address_error,"Address maximum length exceeded");
    }//GEN-LAST:event_txtPatientAddressKeyPressed

    private void txtPatientIdNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPatientIdNumberKeyPressed
        // TODO add your handling code here:
        CommonUtils.checkValidStringWithLength(12,txtPatientIdNumber.getText().toString(),lbl_nic_error,"NIC maximum length exceeded");
    }//GEN-LAST:event_txtPatientIdNumberKeyPressed

    private void txtPatientClinicNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPatientClinicNumberKeyPressed
        // TODO add your handling code here:
        CommonUtils.checkValidStringWithLength(49,txtPatientClinicNumber.getText().toString(),lbl_clinic_error,"Clinic number maximum length exceeded");
    }//GEN-LAST:event_txtPatientClinicNumberKeyPressed

    private void txtPatientContactNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPatientContactNumberKeyPressed
        // TODO add your handling code here:



    }//GEN-LAST:event_txtPatientContactNumberKeyPressed

    private void txtPatientDiagnosisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPatientDiagnosisKeyPressed
        // TODO add your handling code here:
        CommonUtils.checkValidStringWithLength(799, txtPatientDiagnosis.getText().toString(), lbl_error_detail, "Diagnosis maximum length exceeded");
    }//GEN-LAST:event_txtPatientDiagnosisKeyPressed

    private void cmbPatientGenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPatientGenderActionPerformed
        // TODO add your handling code here:
        
        if (this.cmbPatientGender.getSelectedIndex() == 0) {
            lbl_gender_error.setVisible(true);
        } else {
            lbl_gender_error.setVisible(false);
        }
    }//GEN-LAST:event_cmbPatientGenderActionPerformed

    private void txtPatientContactNumberKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPatientContactNumberKeyTyped
        // TODO add your handling code here:
         if (txtPatientContactNumber.getText() != null && !txtPatientContactNumber.getText().toString().isEmpty()) {
            CommonUtils.checkValidNumberWithLength(12, txtPatientContactNumber.getText().toString(), lbl_contact_error, "Contact number maximum length exceeded");

            if (!CommonUtils.checkIsNumberTypeString(txtPatientContactNumber.getText().toString())) {
                lbl_contact_error.setVisible(true);
            }else{
            lbl_contact_error.setVisible(false);
            }
        } else {
            lbl_contact_error.setVisible(false);
        }
    }//GEN-LAST:event_txtPatientContactNumberKeyTyped

    private void txtPatientContactNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPatientContactNumberKeyReleased
        // TODO add your handling code here:
//         if (!CommonUtils.checkIsNumberTypeString(txtPatientContactNumber.getText().toString())) {
//            lbl_contact_error.setVisible(true);
//        }
    }//GEN-LAST:event_txtPatientContactNumberKeyReleased

    private void txtPatientWeightKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPatientWeightKeyReleased
        // TODO add your handling code here:
//        
//        if (!CommonUtils.checkIsNumberTypeString(txtPatientWeight.getText().toString())) {
//            lbl_weight_error.setVisible(true);
//        }else{
//        lbl_weight_error.setVisible(false);
//        }
    }//GEN-LAST:event_txtPatientWeightKeyReleased

    private void btn_clear_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_clear_updateActionPerformed
        // TODO add your handling code here:
        Patient.clearAll(cmbPatientGender);
        Patient.clearAll(txtPatientName);
        Patient.clearAll(txtPatientAddress);
        Patient.clearAll(txtPatientIdNumber);
        Patient.clearAll(txtPatientContactNumber);
        Patient.clearAll(txtPatientWeight);
        Patient.clearAll(txtPatientDiagnosis);
    }//GEN-LAST:event_btn_clear_updateActionPerformed

    private void btn_clear_registerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_clear_registerActionPerformed
        // TODO add your handling code here:
        Patient.clearAll(cmbPatientGender);
        Patient.clearAll(txtPatientName);
        Patient.clearAll(txtPatientAddress);
        Patient.clearAll(txtPatientIdNumber);
        Patient.clearAll(txtPatientClinicNumber);
        Patient.clearAll(txtPatientContactNumber);
        Patient.clearAll(txtPatientWeight);
        Patient.clearAll(txtPatientDiagnosis);
    }//GEN-LAST:event_btn_clear_registerActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PatientsRegistration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PatientsRegistration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PatientsRegistration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PatientsRegistration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new PatientsRegistration().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(PatientsRegistration.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPatientRegister;
    private javax.swing.JButton btnPatientRemove;
    private javax.swing.JButton btnPatientUpdate;
    private javax.swing.JButton btn_clear_register;
    private javax.swing.JButton btn_clear_update;
    private javax.swing.JComboBox<String> cmbPatientGender;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lbl_address_error;
    private javax.swing.JLabel lbl_clinic_error;
    private javax.swing.JLabel lbl_contact_error;
    private javax.swing.JLabel lbl_diagnosis_error1;
    private javax.swing.JLabel lbl_error_detail;
    private javax.swing.JLabel lbl_error_patient_data_load_by_clinic_id;
    private javax.swing.JLabel lbl_gender_error;
    private javax.swing.JLabel lbl_name_error;
    private javax.swing.JLabel lbl_nic_error;
    private javax.swing.JLabel lbl_ok_patient_data_load_by_clinic_id;
    private javax.swing.JLabel lbl_weight_error;
    private javax.swing.JTextArea txtPatientAddress;
    private javax.swing.JTextField txtPatientClinicNumber;
    private javax.swing.JTextField txtPatientContactNumber;
    private javax.swing.JTextArea txtPatientDiagnosis;
    private javax.swing.JTextField txtPatientIdNumber;
    private javax.swing.JTextArea txtPatientName;
    private javax.swing.JTextField txtPatientWeight;
    // End of variables declaration//GEN-END:variables
}
