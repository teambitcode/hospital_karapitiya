/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.interfaces;

import common.CommonIcons;
import common.CommonUtils;
import constants.DrugsTable;
import database.DbConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import models.Drug;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author dilupa
 */
public class MedicineThings extends javax.swing.JFrame {

    Connection conn = null;
    ArrayList<Drug> drugList = new ArrayList<>();
    CommonIcons imageIconObject = new CommonIcons();
    int current_index = 0;
    int current_drug_id = 0;
    String errorMessage = "";
    /**
     * Creates new form MedicineThings
     */
    public MedicineThings() throws SQLException {
        initComponents();
        DbConnection dbConnection = DbConnection.getInstance();
        conn = dbConnection.getConnection();
        btn_update.setVisible(false);
        btn_delete.setVisible(false);
         btn_cancel.setVisible(false);
        btn_add.setVisible(true);
        loadData();
        loadCustomTable();
    }
 public void loadData(){
        try {
            ResultSet rst = Drug.getAllDrugsAsResultSetForTable(conn);
//            drugTable.setModel(DbUtils.resultSetToTableModel(rst));
                drugList.clear();
            while (rst.next()) {
                Drug drug = new Drug();
                drug.setDrugId(rst.getString("DRUG_ID"));
                drug.setDrugName(rst.getString("DRUG_NAME"));
                drug.setVialCount(rst.getInt("VIAL_COUNT"));
//                                drug.setCreatedOn(rst.getString("CREATED_ON"));
//                                drug.setUpdatedOn(rst.getString("UPDATED_ON"));
                this.drugList.add(drug);
            }
                
        } catch (Exception e) {
                System.out.println(e);
        }
    }
    public void resetFrom() {
        txt_drug_name.setText("");
        txt_number_of_vials.setText("");
    }
 public void loadDataByConditon(){
// reservationList
        boolean isEmpty = true;
        try {
            if (txt_drug_name.getText() != null && txt_drug_name.getText().toString() != null && !txt_drug_name.getText().toString().isEmpty()) {
                ResultSet rst = Drug.getPatientsByCondition(conn, DrugsTable.DRUG_NAME, txt_drug_name.getText().toString());
                drugList.clear();
                while (rst.next()) {
                    isEmpty = false;
                    Drug drug = new Drug();
                    drug.setDrugId(rst.getString("DRUG_ID"));
                    drug.setDrugName(rst.getString("DRUG_NAME"));
                    drug.setVialCount(rst.getInt("VIAL_COUNT"));
                    this.drugList.add(drug);
                    loadCustomTable();
                }
                if (isEmpty) {
                    JOptionPane.showMessageDialog(null, "No result found", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please enter search condition", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } catch (Exception e) {
        }
    }
 public void loadCustomTable() {
        drugTable.setModel(new DefaultTableModel(new Object[][]{},
                new String[]{
                    "Drug ID", "Drug Name", "Vial Count"
                }));
        DefaultTableModel model = (DefaultTableModel) drugTable.getModel();

        for (int i = 0; i < drugList.size(); i++) {

            model.addRow(new Object[]{
                drugList.get(i).getDrugId(),
                drugList.get(i).getDrugName(),
                drugList.get(i).getVialCount()
            });
        }
    }

    public Boolean valid() {
        boolean status = true;
        if (!CommonUtils.checkValidStringText(txt_drug_name.getText())) {
            status = false;
            errorMessage = "Please fill all fields";
        }
        if (!CommonUtils.checkValidStringNumberWithZero(txt_number_of_vials.getText())) {
            status = false;
            errorMessage = "Please enter valid vial count";
        }
        if (status) {
            errorMessage = "";
        }
        return status;
    }
    public void loadTableDrugDropDownList(){
        try {
            ResultSet rst = Drug.getAllDrugsAsResultSetForTable(conn);
//            drpdwn_drug.removeAllItems();
            while (rst.next()) {
                Drug drug = new Drug();
                drug.setDrugId(Integer.toString(rst.getInt("DRUG_ID")));
                drug.setDrugName(rst.getString("DRUG_NAME"));
                drug.setVialCount(rst.getInt("VIAL_COUNT"));
//                                drug.setCreatedOn(rst.getString("CREATED_ON"));
//                                drug.setUpdatedOn(rst.getString("UPDATED_ON"));
                this.drugList.add(drug);
//                drpdwn_drug.addItem(drug.getDrugId() + " - " + drug.getDrugName());
            }
                
        } catch (Exception e) {
                System.out.println(e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txt_drug_name = new javax.swing.JTextField();
        txt_drug_search = new javax.swing.JTextField();
        jButton6 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btn_cancel = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        btn_delete = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        lbl_add_val = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        drugTable = new javax.swing.JTable();
        btn_add = new javax.swing.JButton();
        btn_update = new javax.swing.JButton();
        txt_number_of_vials = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(600, 400));
        setPreferredSize(new java.awt.Dimension(1000, 700));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(51, 0, 51));
        jPanel1.setToolTipText("");
        jPanel1.setLayout(null);

        txt_drug_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_drug_nameActionPerformed(evt);
            }
        });
        jPanel1.add(txt_drug_name);
        txt_drug_name.setBounds(230, 110, 530, 28);

        txt_drug_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_drug_searchActionPerformed(evt);
            }
        });
        jPanel1.add(txt_drug_search);
        txt_drug_search.setBounds(330, 310, 300, 28);

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/search_label.png"))); // NOI18N
        jButton6.setText("Search");
        jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton6MouseClicked(evt);
            }
        });
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton6);
        jButton6.setBounds(690, 300, 110, 40);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Search Drug");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(190, 300, 110, 40);

        btn_cancel.setBackground(new java.awt.Color(204, 204, 204));
        btn_cancel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        jPanel1.add(btn_cancel);
        btn_cancel.setBounds(660, 230, 100, 40);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Drug Name");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(120, 120, 80, 17);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Treatment Store");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(340, 30, 310, 50);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/people.png"))); // NOI18N
        jButton2.setText("All Patients");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(60, 40, 150, 40);

        btn_delete.setBackground(new java.awt.Color(255, 168, 168));
        btn_delete.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_delete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/removed.png"))); // NOI18N
        btn_delete.setText("Delete");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });
        jPanel1.add(btn_delete);
        btn_delete.setBounds(460, 230, 110, 40);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/home.png"))); // NOI18N
        jButton3.setText("Home");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(770, 40, 150, 40);

        lbl_add_val.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        jPanel1.add(lbl_add_val);
        lbl_add_val.setBounds(130, 284, 740, 70);

        drugTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        drugTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                drugTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(drugTable);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(130, 360, 740, 240);

        btn_add.setBackground(new java.awt.Color(204, 255, 204));
        btn_add.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/add.png"))); // NOI18N
        btn_add.setText("Add");
        btn_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addActionPerformed(evt);
            }
        });
        jPanel1.add(btn_add);
        btn_add.setBounds(230, 230, 100, 40);

        btn_update.setBackground(new java.awt.Color(255, 253, 203));
        btn_update.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_update.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/update.png"))); // NOI18N
        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });
        jPanel1.add(btn_update);
        btn_update.setBounds(230, 230, 120, 40);

        txt_number_of_vials.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_number_of_vialsActionPerformed(evt);
            }
        });
        jPanel1.add(txt_number_of_vials);
        txt_number_of_vials.setBounds(230, 170, 120, 28);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Number of vials");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(90, 180, 120, 17);

        jLabel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 10, 960, 630);

        jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));
        jPanel1.add(jLabel5);
        jLabel5.setBounds(60, 94, 860, 520);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/walpapers/home.jpg"))); // NOI18N
        jLabel3.setText("jLabel1");
        jLabel3.setMaximumSize(new java.awt.Dimension(900, 600));
        jLabel3.setMinimumSize(new java.awt.Dimension(900, 600));
        jLabel3.setPreferredSize(new java.awt.Dimension(900, 600));
        jPanel1.add(jLabel3);
        jLabel3.setBounds(0, 0, 1000, 800);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1000, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1000, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 800, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 800, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_drug_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_drug_nameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_drug_nameActionPerformed

    private void txt_drug_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_drug_searchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_drug_searchActionPerformed

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
        // TODO add your handling code here:
        Drug newDrug = new Drug();

        Boolean deletePatient = newDrug.deleteDrugById(conn, current_drug_id);
        if (deletePatient == null) {
            JOptionPane.showMessageDialog(null, "This drug already used. Cannot remove this drug.", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            if (deletePatient == false) {
                JOptionPane.showMessageDialog(null, "Successfully removed", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
                loadData();
                loadCustomTable();
                btn_cancel.setVisible(false);
                btn_add.setVisible(true);
                txt_drug_search.setEnabled(true);
                resetFrom();
                
            } else {
                JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
                loadData();
                loadCustomTable();
            }
            btn_update.setVisible(false);
            btn_delete.setVisible(false);
        }
        
    }//GEN-LAST:event_btn_deleteActionPerformed

    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
        // TODO add your handling code here:
        
        if (valid()) {
            Drug newDrug = new Drug();
            newDrug.setDrugName(this.txt_drug_name.getText().toString());
            newDrug.setVialCount(Integer.parseInt(this.txt_number_of_vials.getText().toString()));

            Boolean insertPatient = newDrug.insertDrug(conn);
            if (!insertPatient) {
                JOptionPane.showMessageDialog(null, "Successfully added", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
                loadData();
                loadCustomTable();
                resetFrom();
            } else {
                JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
                loadData();
                loadCustomTable();
            }
        } else {
            JOptionPane.showMessageDialog(null,errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
        }

        
    }//GEN-LAST:event_btn_addActionPerformed

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
        // TODO add your handling code here:
        if(valid()){
        boolean status = false;
        Drug drug = new Drug();
        HashMap<String, Object> drugData = new HashMap<>();
//        drugData.put(DrugsTable.DRUG_ID, current_drug_id);
        drugData.put(DrugsTable.DRUG_NAME, this.txt_drug_name.getText().toString());
        drugData.put(DrugsTable.VIAL_COUNT, Integer.parseInt(this.txt_number_of_vials.getText().toString()));

        status = drug.updateDrug(conn, drugData, current_drug_id);
        if (status) {
            JOptionPane.showMessageDialog(null, "Successfully updated", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
            loadData();
            loadCustomTable();
            loadTableDrugDropDownList();
            btn_cancel.setVisible(false);
            btn_add.setVisible(true);
            btn_update.setVisible(false);
            btn_delete.setVisible(false);
            txt_drug_search.setEnabled(true);
            resetFrom();
        } else {

            JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
        }
        }else{
        JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_btn_updateActionPerformed

    private void txt_number_of_vialsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_number_of_vialsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_number_of_vialsActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
      Home home;
        try {
            home = new Home();
            home.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(MedicalTreatments.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
      AllPatients allPatients;
        try {
            allPatients = new AllPatients();
            allPatients.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void drugTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drugTableMouseClicked
        // TODO add your handling code here:
        txt_drug_search.setEnabled(false);
        btn_cancel.setVisible(true);
        btn_add.setVisible(false);
        btn_update.setVisible(true);
        btn_delete.setVisible(true);
        int row = drugTable.getSelectedRow();
        current_index = row + 1;
        txt_number_of_vials.setText(drugTable.getValueAt(row, 2).toString());
        txt_drug_name.setText(drugTable.getValueAt(row, 1).toString());
        current_drug_id = Integer.parseInt(drugTable.getValueAt(row, 0).toString());
    }//GEN-LAST:event_drugTableMouseClicked

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        // TODO add your handling code here:
        btn_cancel.setVisible(false);
        btn_delete.setVisible(false);
        btn_update.setVisible(false);
        btn_add.setVisible(true);
        current_drug_id = -1;
        txt_number_of_vials.setText("");
        txt_drug_name.setText("");
        txt_drug_search.setEnabled(true);
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void jButton6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseClicked
        // TODO add your handling code here:
        //        432423CLINICID123
          if (txt_drug_search.getText() != null && (!txt_drug_search.getText().toString().equals("")) && (!txt_drug_search.getText().toString().isEmpty())) {
            loadDataByConditon();
        } else {
            loadData();
            loadCustomTable();
        }
    }//GEN-LAST:event_jButton6MouseClicked

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        if (txt_drug_search.getText() != null && (!txt_drug_search.getText().toString().equals("")) && (!txt_drug_search.getText().toString().isEmpty())) {
            loadDataByConditon();
        } else {
            loadData();
            loadCustomTable();
        }

    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MedicineThings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MedicineThings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MedicineThings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MedicineThings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new MedicineThings().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(MedicineThings.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add;
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_update;
    private javax.swing.JTable drugTable;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_add_val;
    private javax.swing.JTextField txt_drug_name;
    private javax.swing.JTextField txt_drug_search;
    private javax.swing.JTextField txt_number_of_vials;
    // End of variables declaration//GEN-END:variables
}
