/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.interfaces;

import common.CrudOperations;
import constants.DrugsTable;
import constants.PatientTable;
import constants.TabelNames;
import database.DbConnection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import models.Drug;
import models.DrugReservation;
import models.Patient;
import net.proteanit.sql.DbUtils;
import common.ComboItem;
import common.CommonIcons;
import common.CommonUtils;
import java.awt.Color;
import java.util.List;
import java.text.SimpleDateFormat;
import java.time.format.FormatStyle;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import javax.swing.JButton;
import models.DrugReservationPerPatientData;
/**
 *
 * @author dilupa
 */
public class MedicalTreatments extends javax.swing.JFrame {

    Connection conn = null;
    String clinicId = null;
    ArrayList<DrugReservation> reservationList = new ArrayList<>();
    ArrayList<Drug> drugList = new ArrayList<>();
    int current_index = 0;
    String patient_id = null;
    int currentGroupNumber;
    String errorMessage = "";
    CommonIcons imageIconObject = new CommonIcons();
    int resId = 0;
    /**
     * Creates new form MedicalTreatments
     */
    public MedicalTreatments(String clinicId) throws SQLException {
        this.clinicId = clinicId;
        initComponents();
        reset();
        DbConnection dbConnection = DbConnection.getInstance();
        conn = dbConnection.getConnection();
//        getAllDrugs();
        lbl_ok_patient_data_load_by_clinic_id.setVisible(false);
        lbl_error_patient_data_load_by_clinic_id.setVisible(false);
//        if (getPatientClinicId() != null) {
//            loadCurrentPatientData();
//        }

        loadCashedReservedTable();
        getAllDrugs();
        getCurrentMabNumber();
        getLastTreatmentDate();
        setButtonEnable(btn_add, true);
        setButtonEnable(btn_update, false);
        setButtonEnable(btn_delete, false);

        CommonUtils.resetDateChooserToCurrentDate(jDateChooser1);
  
    }

    public void reset() {
        drpd_drugList.setSelectedIndex(0);
//        txt_mab_number.setText("");
        setButtonEnable(btn_update, false);
        setButtonEnable(btn_delete, false);

    }

    public Boolean valid() {
        if (!CommonUtils.checkValidStringNumber(txt_mab_number.getText())) {
            errorMessage = "Please enter valid MAB number";
            return false;
        } else {
            errorMessage = "";
            return true;
        }

    }

    public MedicalTreatments() throws SQLException {
        initComponents();
        DbConnection dbConnection = DbConnection.getInstance();
        conn = dbConnection.getConnection();

        reset();
        lbl_ok_patient_data_load_by_clinic_id.setVisible(false);
        lbl_error_patient_data_load_by_clinic_id.setVisible(false);
//        if (getPatientClinicId() != null) {
//            loadCurrentPatientData();
//        }

        loadCashedReservedTable();
        getAllDrugs();
        getLastTreatmentDate();
        setButtonEnable(btn_give_treatment, false);

//        setButtonEnable(btn_save, true);
        setButtonEnable(btn_give_treatment, false);
        CommonUtils.resetDateChooserToCurrentDate(jDateChooser1);
    }
    
    public void tempData(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        LocalDateTime now = LocalDateTime.now();
//        reservationList.add(new DrugReservation(1, "New drug 230mg",now,"PENDING", clinicId));
//        reservationList.add(new DrugReservation(2, "old drug 120mg",now,"PENDING", clinicId));
//        reservationList.add(new DrugReservation(3, "my drug 20mg",now,"PENDING", clinicId));
        
    }
    public LocalDateTime convertToLocalDateViaInstant(String dateToConvert) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(dateToConvert, formatter);
        return dateTime;
}
    public Boolean checkDrugAvailability(String id, int count){
        getDrugList();
        for (int i = 0; i < drugList.size(); i++) {
            if (drugList.get(i).getDrugId().equals(id)) {
                if ((drugList.get(i).getVialCount() - count) < 0) {
//            break;
                    errorMessage = drugList.get(i).getDrugName()+ " drug is out of stock";
                    return false;
                } else {
                    return true;
                }
            }
        }
        errorMessage = id+ " drug id not found";
        return null;
    }
    
    public void getDrugList(){
      drugList = DrugReservationPerPatientData.drugsAsArrayList(conn, clinicId);
    }
    
    public Boolean checkSelectedAllDrugsAvailability() {
        HashMap<String, Integer> drugData = getSelectedDrugsWithCount();
        boolean status = false;

        for (Map.Entry<String, Integer> entry : drugData.entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue());
            boolean check = checkDrugAvailability(entry.getKey(), entry.getValue());
            if (check) {
                status = true;
            } else {
                return false;
            }
        }    
    return status;
    }
    
    public HashMap<String, Integer> getSelectedDrugsWithCount(){
      HashMap<String, Integer> drugData = new HashMap<>();
      
        for (int i = 0; i < reservationList.size(); i++) {
            if (!reservationList.get(i).getStatus().equals("DELETED")) {
                if (drugData.get(reservationList.get(i).getDrug_id()) != null) {
                    int count = drugData.get(reservationList.get(i).getDrug_id());
                    drugData.put(reservationList.get(i).getDrug_id(), count + 1);
                } else {
                    drugData.put(reservationList.get(i).getDrug_id(), 1);
                }
            }

        }
      return drugData;
    }
    public void loadSavedDrugReservation() {
        String sql = "SELECT * FROM drug_reservation dr, patient p , drugs d where p.CLINIC_ID='"+clinicId+"' and dr.PATIENT_ID = p.CLINIC_ID and d.DRUG_ID = dr.DRUG_ID and dr.STATUS <> 'COMPLETED';";
        ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);

        HashMap<String, String> patientData = new HashMap<>();

        try {
            while (rst.next()) {
                patientData.put("DRUG_ID", rst.getString("DRUG_ID"));
                patientData.put("DRUG_NAME", rst.getString("DRUG_NAME"));
                patientData.put("MAB_NUMBER", Integer.toString(rst.getInt("MAB_NUMBER")));
                patientData.put("GROUP_NUMBER", Integer.toString(rst.getInt("GROUP_NUMBER")));
                this.currentGroupNumber = rst.getInt("GROUP_NUMBER");
                patientData.put("CREATED_ON_DATE", rst.getString("CREATED_ON_DATE"));
                patientData.put("CLINIC_ID", rst.getString("CLINIC_ID"));
                patientData.put("STATUS", rst.getString("STATUS"));
                patientData.put("DRUG_RESERVATION_ID",  Integer.toString(rst.getInt("DRUG_RESERVATION_ID")));
                String stringDat = patientData.get("CREATED_ON_DATE");
                LocalDateTime newDate = convertToLocalDateViaInstant(stringDat);
                 
                reservationList.add(new DrugReservation(
                        Integer.parseInt(patientData.get("MAB_NUMBER")),
                        patientData.get("DRUG_NAME"),
                        patientData.get("DRUG_ID"),
                        newDate,
                        patientData.get("STATUS"),
                        patientData.get("CLINIC_ID"),
                        Integer.parseInt(patientData.get("DRUG_RESERVATION_ID")),
                        Integer.parseInt(patientData.get("GROUP_NUMBER"))));
                setButtonEnable(btn_give_treatment, true);
            }
            loadCashedReservedTable();

        } catch (Exception e) {
            System.out.println(e);
        }

    }
    public void loadCashedReservedTable() {
        reservedTable.setModel(new DefaultTableModel( new Object [][] {},
            new String [] {
                "Day","Drug", "MAB Number", "Date and Time", "Status","RES ID"
                }));
        DefaultTableModel model = (DefaultTableModel) reservedTable.getModel();
        
        for (int i = 0; i < reservationList.size(); i++) {
            int h = reservationList.get(i).getReservation_id();
            System.out.println(reservationList.get(i).getReservation_id());
            if (!reservationList.get(i).getStatus().equals("DELETED")){
        model.addRow(new Object[]{
                    reservationList.get(i).getGroup_number(),
                    reservationList.get(i).getDrug(),
                    reservationList.get(i).getMabNumber(),
                    reservationList.get(i).getDate(),
                    reservationList.get(i).getStatus(),
                    reservationList.get(i).getReservation_id()});
            }


        }
        getCurrentMabNumber();
    }

    public void getAllDrugs() {
        String sql = "SELECT *, drp.VIAL_COUNT as P_VIAL_COUNT FROM DRUGS d,DRUG_RESERVATION_PER_PATIENT drp WHERE drp.CLINIC_ID='" + clinicId + "' and d.DRUG_ID = drp.DRUG_ID";

ResultSet drugResultSet = CrudOperations.executeCustomQuary(conn, sql);
        try {
            drugList.clear();
            drpd_drugList.removeAllItems();
            while (drugResultSet.next()) {
                String id = drugResultSet.getString(DrugsTable.DRUG_ID);
                if (checkDuplicateDrugs(drugList, id)) {
                    Drug drugItem = new Drug();
                    drugItem.setDrugId(drugResultSet.getString(DrugsTable.DRUG_ID));
                    drugItem.setDrugName(drugResultSet.getString(DrugsTable.DRUG_NAME));
                    drugItem.setVialCount(drugResultSet.getInt("P_VIAL_COUNT"));
                    drugItem.setCreatedOn(drugResultSet.getString(DrugsTable.CREATED_ON));
                    drugItem.setUpdatedOn(drugResultSet.getString(DrugsTable.UPDATED_ON));
                    this.drugList.add(drugItem);
                    drpd_drugList.addItem(drugResultSet.getString(DrugsTable.DRUG_ID) + "-" + drugResultSet.getString(DrugsTable.DRUG_NAME));
                }

            }
            if(this.drugList.size() > 0){
                setDrugStockLabel(jLabel1, this.drugList.get(0).getVialCount());
                Drug.checkDrugAvailability(this.drugList.get(0).getDrugId(), this.drugList, jLabel1);
            }
//            this.drpd_drugList.setModel((Drug<String>) drugList);
        } catch (Exception e) {
        }
    }
    public Boolean checkDuplicateDrugs(ArrayList<Drug> drugList, String drugId) {
        List<Drug> hasDuplicatesList = drugList.stream()
                .filter(p -> p.getDrugId().equals(drugId)).collect(Collectors.toList());

            return hasDuplicatesList.size() > 0 ? false : true;
    }
    public void setDrugStockLabel(JLabel label, int count) {
        label.setText("Stock : "+count);
    }
    public void loadPatientDataByClinicId() {
        clinicId = txt_clinic_id.getText().toString();
        loadCurrentPatientData(true);
    }

    public void setButtonEnable(JButton buttonName, boolean status) {
        buttonName.setEnabled(status);

    }
    public void loadCurrentPatientData(Boolean showResponse) {
        try {
            ResultSet rst = Patient.getOnePatientById(conn, clinicId);
            HashMap<String, Object> tableFieldsMap = new HashMap<>();

            tableFieldsMap = CrudOperations.getMapFromResultSet(rst, TabelNames.PATIENT);
            if (tableFieldsMap.size() > 0) {
                lbl_name.setText(tableFieldsMap.get(PatientTable.NAME).toString());
                lbl_clinic_number.setText(tableFieldsMap.get(PatientTable.CLINIC_ID).toString());
                lbl_id_number.setText(tableFieldsMap.get(PatientTable.ID_NUMBER).toString());
//                lbl_ .setText(tableFieldsMap.get(PatientTable.CLINIC_ID).toString());
//                lbl_clinic_number.setText(tableFieldsMap.get(PatientTable.CLINIC_ID).toString());
//                lbl_clinic_number.setText(tableFieldsMap.get(PatientTable.CLINIC_ID).toString());
                System.out.println(tableFieldsMap);

                showHideLables(lbl_ok_patient_data_load_by_clinic_id);
            } else {
                JOptionPane.showMessageDialog(null, "No patient record found!", "Error", JOptionPane.ERROR_MESSAGE);
                showHideLables(lbl_error_patient_data_load_by_clinic_id);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void showHideLables(JLabel labelName) throws InterruptedException {
        labelName.setVisible(true);
//        Thread.currentThread().sleep(4000);
//        labelName.setVisible(false);
        int delay = 5000; //milliseconds
        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                labelName.setVisible(false);
            }
        };
        new javax.swing.Timer(delay, taskPerformer).start();
    }

    public void setLabelName(String name) {
        this.lbl_name.setText(name);
    }

    public void setPatientClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getPatientClinicId() {
        return this.clinicId;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lbl_name = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lbl_clinic_number = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lbl_id_number = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lbl_last_treatment_date = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        lbl_last_treatment = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        drpd_drugList = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        lbl_ok_patient_data_load_by_clinic_id = new javax.swing.JLabel();
        txt_clinic_id = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel8 = new javax.swing.JLabel();
        lbl_error_patient_data_load_by_clinic_id = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        reservedTable = new javax.swing.JTable();
        jLabel20 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        btn_give_treatment = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        btn_add = new javax.swing.JButton();
        btn_update = new javax.swing.JButton();
        btn_delete = new javax.swing.JButton();
        txt_mab_number = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1000, 700));
        setPreferredSize(new java.awt.Dimension(1000, 700));
        setResizable(false);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(51, 0, 51));
        jPanel1.setMinimumSize(new java.awt.Dimension(1000, 800));
        jPanel1.setPreferredSize(new java.awt.Dimension(1000, 800));
        jPanel1.setLayout(null);

        lbl_name.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_name.setForeground(new java.awt.Color(255, 255, 255));
        lbl_name.setText("--------------");
        jPanel1.add(lbl_name);
        lbl_name.setBounds(210, 160, 280, 17);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Treatments");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(360, 30, 230, 50);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Name");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(90, 160, 70, 17);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Clinic Number");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(90, 190, 100, 17);

        lbl_clinic_number.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_clinic_number.setForeground(new java.awt.Color(255, 255, 255));
        lbl_clinic_number.setText("--------------");
        jPanel1.add(lbl_clinic_number);
        lbl_clinic_number.setBounds(210, 190, 310, 17);

        jLabel1.setFont(new java.awt.Font("UD Digi Kyokasho NK-R", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Stock : 100");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(170, 380, 140, 20);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("ID Number");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(90, 220, 80, 17);

        lbl_id_number.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_id_number.setForeground(new java.awt.Color(255, 255, 255));
        lbl_id_number.setText("--------------");
        jPanel1.add(lbl_id_number);
        lbl_id_number.setBounds(210, 220, 320, 17);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Last Treatment Date");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(560, 160, 150, 17);

        lbl_last_treatment_date.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_last_treatment_date.setForeground(new java.awt.Color(255, 255, 255));
        lbl_last_treatment_date.setText("--------------");
        jPanel1.add(lbl_last_treatment_date);
        lbl_last_treatment_date.setBounds(730, 160, 150, 17);

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Last Treament MAB");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(560, 180, 160, 17);

        lbl_last_treatment.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_last_treatment.setForeground(new java.awt.Color(255, 255, 255));
        lbl_last_treatment.setText("--------------");
        jPanel1.add(lbl_last_treatment);
        lbl_last_treatment.setBounds(730, 180, 150, 17);
        jPanel1.add(jSeparator1);
        jSeparator1.setBounds(50, 280, 890, 10);

        drpd_drugList.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        drpd_drugList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                drpd_drugListActionPerformed(evt);
            }
        });
        jPanel1.add(drpd_drugList);
        drpd_drugList.setBounds(170, 340, 200, 28);

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("MAB Number");
        jPanel1.add(jLabel15);
        jLabel15.setBounds(400, 350, 100, 17);

        lbl_ok_patient_data_load_by_clinic_id.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/save.png"))); // NOI18N
        jPanel1.add(lbl_ok_patient_data_load_by_clinic_id);
        lbl_ok_patient_data_load_by_clinic_id.setBounds(440, 100, 40, 40);

        txt_clinic_id.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_clinic_idActionPerformed(evt);
            }
        });
        jPanel1.add(txt_clinic_id);
        txt_clinic_id.setBounds(180, 110, 200, 28);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/load_data_large.png"))); // NOI18N
        jButton1.setText("Load Last Treatment");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(400, 290, 200, 30);

        jDateChooser1.setBackground(new java.awt.Color(204, 255, 204));
        jPanel1.add(jDateChooser1);
        jDateChooser1.setBounds(170, 290, 200, 28);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Date");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(120, 300, 60, 17);

        lbl_error_patient_data_load_by_clinic_id.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/warning.png"))); // NOI18N
        jPanel1.add(lbl_error_patient_data_load_by_clinic_id);
        lbl_error_patient_data_load_by_clinic_id.setBounds(440, 100, 40, 40);

        reservedTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        reservedTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                reservedTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(reservedTable);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(110, 480, 770, 140);

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/search_label.png"))); // NOI18N
        jLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel20MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel20);
        jLabel20.setBounds(390, 100, 40, 50);

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Treatment");
        jPanel1.add(jLabel17);
        jLabel17.setBounds(80, 350, 80, 17);

        btn_give_treatment.setFont(new java.awt.Font("Verdana Pro Semibold", 1, 14)); // NOI18N
        btn_give_treatment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/injection.png"))); // NOI18N
        btn_give_treatment.setText("Give Treatment");
        btn_give_treatment.setVerifyInputWhenFocusTarget(false);
        btn_give_treatment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_give_treatmentActionPerformed(evt);
            }
        });
        jPanel1.add(btn_give_treatment);
        btn_give_treatment.setBounds(660, 220, 220, 50);

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Clinic No.");
        jPanel1.add(jLabel18);
        jLabel18.setBounds(90, 110, 80, 17);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/people.png"))); // NOI18N
        jButton2.setText("All Patients");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(50, 40, 150, 40);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/home.png"))); // NOI18N
        jButton3.setText("Home");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(710, 40, 110, 40);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/store_history_icon.png"))); // NOI18N
        jButton4.setText("History");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);
        jButton4.setBounds(830, 40, 110, 40);

        btn_add.setBackground(new java.awt.Color(204, 255, 204));
        btn_add.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/add.png"))); // NOI18N
        btn_add.setText("Add");
        btn_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addActionPerformed(evt);
            }
        });
        jPanel1.add(btn_add);
        btn_add.setBounds(170, 420, 100, 40);

        btn_update.setBackground(new java.awt.Color(255, 253, 203));
        btn_update.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_update.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/update.png"))); // NOI18N
        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });
        jPanel1.add(btn_update);
        btn_update.setBounds(290, 420, 120, 40);

        btn_delete.setBackground(new java.awt.Color(255, 168, 168));
        btn_delete.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_delete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/removed.png"))); // NOI18N
        btn_delete.setText("Delete");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });
        jPanel1.add(btn_delete);
        btn_delete.setBounds(430, 420, 110, 40);

        txt_mab_number.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_mab_numberActionPerformed(evt);
            }
        });
        jPanel1.add(txt_mab_number);
        txt_mab_number.setBounds(500, 340, 70, 28);

        jLabel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));
        jPanel1.add(jLabel4);
        jLabel4.setBounds(50, 100, 890, 540);

        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/walpapers/home.jpg"))); // NOI18N
        jLabel19.setText("jLabel1");
        jLabel19.setMaximumSize(new java.awt.Dimension(900, 600));
        jLabel19.setMinimumSize(new java.awt.Dimension(900, 600));
        jLabel19.setPreferredSize(new java.awt.Dimension(900, 600));
        jPanel1.add(jLabel19);
        jLabel19.setBounds(0, 0, 1000, 800);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1000, 800);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_clinic_idActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_clinic_idActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_clinic_idActionPerformed

    private void txt_mab_numberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_mab_numberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_mab_numberActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        Home home;
        try {
            home = new Home();
            home.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(MedicalTreatments.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        
//          MedicalTreatments medicalTreatments = new MedicalTreatments(id);
//        medicalTreatments.setPatientClinicId(id);
//        medicalTreatments.setVisible(true);
//        medicalTreatments.loadCurrentPatientData(false);
if(clinicId !=null){
    try {
        
        PatientHistory patientHistory = new PatientHistory(clinicId);
        patientHistory.setPatientClinicId(clinicId);
        patientHistory.setVisible(true);
        patientHistory.loadSinglePatientHistory(true);
        patientHistory.getLastTreatmentDate();
        patientHistory.setAllMessageIconsFalse();
        patientHistory.setDateSelectable(true);
        this.dispose();
    } catch (SQLException ex) {
        Logger.getLogger(MedicalTreatments.class.getName()).log(Level.SEVERE, null, ex);
    }
            
}else{
                JOptionPane.showMessageDialog(null, "Please select patient", "Error", JOptionPane.ERROR_MESSAGE);
}
       
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        AllPatients allPatients;
        try {
            allPatients = new AllPatients();
            allPatients.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed
    private int getMaxGroupNumber(int mabId){
    int maxGroupNumber = 0;
    String sql = "SELECT MAX(GROUP_NUMBER) as MAX_GROUP FROM drug_reservation dr, patient p , drugs d where p.CLINIC_ID='"+clinicId+"' and dr.PATIENT_ID = p.CLINIC_ID and dr.MAB_NUMBER="+mabId+";";
        ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);
        try {
            while (rst.next()) {
               maxGroupNumber = rst.getInt("MAX_GROUP");
            }
        } catch (SQLException ex) {
            Logger.getLogger(MedicalTreatments.class.getName()).log(Level.SEVERE, null, ex);
        }
    return maxGroupNumber+1;
    }
    private void getLastTreatmentDate(){
        String sql = "SELECT  MAX(dr.CREATED_ON_DATE) as CREATED_ON_DATE  FROM drug_reservation dr, patient p where p.CLINIC_ID='"+clinicId+"' and dr.PATIENT_ID = p.CLINIC_ID and dr.STATUS = 'COMPLETED'  ORDER BY dr.GROUP_NUMBER;";
        ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);
        try {
            while (rst.next()) {
                Date startDate = rst.getDate("CREATED_ON_DATE");
                SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");// FOrmat in This Format or you change Change as well 
                String Format = format.format(startDate);
                System.out.println(Format);// 01/06/2016

                this.lbl_last_treatment_date.setText(Format);
            }
        } catch (Exception e) {
        }
//    
    }
    private void getCurrentMabNumber() {
        if(clinicId != null && !clinicId.isEmpty()){
        boolean isEmpty = true;
        String sql = "SELECT MAX(dr.MAB_NUMBER) as CURRENT_MAB  FROM drug_reservation dr, patient p where p.CLINIC_ID='" + clinicId + "' and dr.PATIENT_ID = p.CLINIC_ID and dr.STATUS = 'COMPLETED'  ORDER BY dr.GROUP_NUMBER;";
        ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);
        try {
            while (rst.next()) {
                isEmpty = false;
                int o = rst.getInt("CURRENT_MAB");
                String p = rst.getString("CURRENT_MAB");
                if (rst.getInt("CURRENT_MAB") == 0) {
                    this.txt_mab_number.setText(Integer.toString(1));
                } else {
                    this.txt_mab_number.setText(Integer.toString(rst.getInt("CURRENT_MAB")));
                }
                lbl_last_treatment.setText(Integer.toString(rst.getInt("CURRENT_MAB")));
            }
            if(isEmpty){
                this.txt_mab_number.setText(Integer.toString(1));   
            }
        } catch (Exception e) {
        }
        }
        
//    
    }
    public LocalDateTime getDateFromChooser() {
        java.util.Date startTypeDate = jDateChooser1.getDate();
        LocalDateTime ldt = null;
        if (CommonUtils.checkDate(startTypeDate)) {
            ldt = LocalDateTime.ofInstant(startTypeDate.toInstant(),
                    ZoneId.systemDefault());
        }
        return ldt;
    }
    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
        // TODO add your handling code here:
        if (valid()) {
                         jDateChooser1.setBackground(Color.WHITE);
            String drug = this.drpd_drugList.getSelectedItem().toString();
            int cycle = Integer.parseInt(this.txt_mab_number.getText().toString());
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
//            LocalDateTime now = LocalDateTime.now();
            
            LocalDateTime now = getDateFromChooser();
            if(now != null){
            String[] values = drug.split("-");
            int groupNumber;
            if (this.reservationList.size() > 0) {
                groupNumber = this.currentGroupNumber;
            } else {
                groupNumber = getMaxGroupNumber(cycle);
            }
            currentGroupNumber = groupNumber;
            reservationList.add(new DrugReservation(cycle, values[1], values[0], now, "PENDING", clinicId, resId, groupNumber));
            resId += 1;
            loadCashedReservedTable();
            reset();
//            getCurrentMabNumber();
            this.txt_mab_number.setText(Integer.toString(cycle));
            setButtonEnable(btn_give_treatment, false);
            }else{
             jDateChooser1.setBackground(Color.RED);
             JOptionPane.showMessageDialog(null, "Please enter valid date", "Error", JOptionPane.ERROR_MESSAGE);
            }
            
        } else {
            JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btn_addActionPerformed

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
        // TODO add your handling code here:
        String drug = this.drpd_drugList.getSelectedItem().toString();
        String[] values = drug.split("-");
        int MabNumber = Integer.parseInt(this.txt_mab_number.getText().toString());
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
//        LocalDateTime now = LocalDateTime.now();
        LocalDateTime now = getDateFromChooser();
        
        if (now != null) {
            if (current_index != 0) {
                reservationList.get(current_index - 1).setMabNumber(MabNumber);
                reservationList.get(current_index - 1).setDrug(values[1]);
                reservationList.get(current_index - 1).setDrug_id(values[0]);
                reservationList.get(current_index - 1).setDate(now);
//            reservationList.get(current_index - 1).setStatus("PENDING");

            }
            loadCashedReservedTable();
            reset();
            setButtonEnable(btn_give_treatment, true);
        } else {
            jDateChooser1.setBackground(Color.RED);
            JOptionPane.showMessageDialog(null, "Please enter valid date", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btn_updateActionPerformed

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
        // TODO add your handling code here:
        if (current_index != 0) {
            reservationList.remove(current_index - 1);
        }
        loadCashedReservedTable();
        reset();
        if (reservationList.size() <= 0) {
            setButtonEnable(btn_give_treatment, false);
        }
    }//GEN-LAST:event_btn_deleteActionPerformed

    private void jLabel20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel20MouseClicked
        // TODO add your handling code here:
        clinicId = txt_clinic_id.getText().toString();
        getCurrentMabNumber();
        if (txt_clinic_id.getText() != null && (!txt_clinic_id.getText().toString().isEmpty())) {
            loadPatientDataByClinicId();
            reservationList.clear();
//            currentGroupNumber = getMaxGroupNumber(1);
//            loadCacheTreatments();
            getAllDrugs();
//                    getCurrentMabNumber();
        } else {
            JOptionPane.showMessageDialog(null, "Please enter the Clinic ID", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_jLabel20MouseClicked
    private Integer getCurrentIndex(int row){
//        int row = reservedTable.getSelectedRow();
        current_index = row;
        int resId = Integer.parseInt(reservedTable.getValueAt(row, 5).toString());
        for (int i = 0; i < reservationList.size(); i++) {
            if (reservationList.get(i).getReservation_id() == resId) {
                current_index = i +1;
            }
        }

        return current_index;
    }
    private String getDropDownDrugName(String name){
    String drugNameInDrp = name;
    for (int i = 0; i < drugList.size(); i++) {
        String j =drugList.get(i).getDrugName();
            if (drugList.get(i).getDrugName().equals(name)) {
                drugNameInDrp =  drugList.get(i).getDrugId()+"-"+drugList.get(i).getDrugName();
                break;
            }
        }
    return drugNameInDrp;
    }
    private void reservedTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reservedTableMouseClicked
        // TODO add your handling code here:
        int row = reservedTable.getSelectedRow();
        current_index = getCurrentIndex(row);
        String drug = reservedTable.getValueAt(row, 1).toString();
        int cycle = Integer.parseInt(reservedTable.getValueAt(row, 2).toString());
        String drpDrugName = getDropDownDrugName(drug);
        drpd_drugList.setSelectedItem(drpDrugName);
        txt_mab_number.setText(Integer.toString(cycle));
        
        setButtonEnable(btn_add, true);
        setButtonEnable(btn_update, true);
        setButtonEnable(btn_delete, true);
    }//GEN-LAST:event_reservedTableMouseClicked

    private Boolean cacheTreatments() {
        String drugList = "";
        for (int i = 0; i < reservationList.size(); i++) {
            drugList += reservationList.get(i).getDrug_id() + "~";
        }
        drugList = drugList.substring(0, drugList.length() - 1);
        if (insertToCacheTreatments(drugList)) {
            return true;
        } else {
            return false;
        }
    }
    private void convertToDrugList(ResultSet rst){
        try {
            int cycle = Integer.parseInt(this.txt_mab_number.getText().toString());
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
            LocalDateTime now = LocalDateTime.now();
            if (rst != null) {
                String drugId = rst.getString("DRUG_ID");
                String drugName = rst.getString("DRUG_NAME");
                int reId = Integer.parseInt(rst.getString("ID"));
                int drugCount = Integer.parseInt(rst.getString("VIAL_COUNT"));
                int groupNumber;
                if (this.reservationList.size() > 0) {
                    groupNumber = this.currentGroupNumber;
                } else {
                    groupNumber = getMaxGroupNumber(cycle);
                }
                currentGroupNumber = groupNumber;
                reservationList.add(new DrugReservation(cycle, drugName, drugId, now, "PENDING", clinicId, reId, groupNumber));
                
            } else {
                JOptionPane.showMessageDialog(null, "No saved treatments!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
        }

            


           
    }
    private Boolean loadCacheTreatments() {
        boolean isEmpty = true;
        String sql = "SELECT *, drp.VIAL_COUNT as P_VIAL_COUNT FROM DRUGS d,DRUG_RESERVATION_PER_PATIENT drp, PATIENT p WHERE drp.CLINIC_ID='" + clinicId + "' and d.DRUG_ID = drp.DRUG_ID and p.CLINIC_ID = drp.CLINIC_ID;";
        ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);

        try {
            reservationList.clear();
            while (rst.next()) {
                isEmpty = false;
                convertToDrugList(rst);
                if (reservationList.size() > 0) {
                    setButtonEnable(btn_give_treatment, true);
                }

            }

            if (isEmpty == true) {
                JOptionPane.showMessageDialog(null, "No records found", "Error", JOptionPane.ERROR_MESSAGE);
                return !isEmpty;
            } else {
                loadCashedReservedTable();
                JOptionPane.showMessageDialog(null, "saved drug list loaded succesfull", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
                return !isEmpty;
            }
        } catch (Exception e) {
            return false;
        }
    }
    private Boolean insertToCacheTreatments(String drugIdList) {
        boolean hasError = true;
        String sql = "INSERT INTO patient_current_treatment_store (CLINIC_ID, DRUG_IDS) VALUES( '" + clinicId + "', '" + drugIdList + "') ON DUPLICATE KEY UPDATE CLINIC_ID='" + clinicId + "', DRUG_IDS='" + drugIdList + "';";
        hasError = CrudOperations.executeCustomQuaryFromExecute(conn, sql);
        
        try {

            if (hasError == true) {
                JOptionPane.showMessageDialog(null, "Druglist saving error for future usage", "Error", JOptionPane.ERROR_MESSAGE);
                return !hasError;
            } else {
                JOptionPane.showMessageDialog(null, "Drug list saved for future usage", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
                return !hasError;
            }
        } catch (Exception e) {
            return false;
        }
    }
    private void btn_give_treatmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_give_treatmentActionPerformed
        // TODO add your handling code here:
        if (clinicId != null && (!clinicId.isEmpty())) {
            boolean status = checkSelectedAllDrugsAvailability();
            if (status) {
                DrugReservation newDurgReservation = new DrugReservation();
                Boolean insertBatchDurgReservation = reservationList.size() != 0
                        ? newDurgReservation.insertBatchDrugReservation(conn, reservationList, "COMPLETED") : true;
                if (insertBatchDurgReservation) {
                    JOptionPane.showMessageDialog(null, "Successfully Reserved", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
                    reservationList.clear();
                    getAllDrugs();
//                    loadCacheTreatments();
                    loadCashedReservedTable();
                    setButtonEnable(btn_give_treatment, false);
                }
            } else {
                JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
//        DrugReservation newDurgReservation = new DrugReservation();
//        ArrayList<DrugReservation> newReservationList = new ArrayList<>();
//        ArrayList<DrugReservation> newUpdateReservationList = new ArrayList<>();
//        for (int i = 0; i < reservationList.size(); i++) {
//            reservationList.get(i).setStatus("COMPLETED");
//            newUpdateReservationList.add(reservationList.get(i));
//        }
//        Boolean updateReservationList = newUpdateReservationList.size() != 0
//                ? newDurgReservation.updateBatchDrugReservation(conn, newUpdateReservationList) : true;
//
//        if (updateReservationList) {
//            JOptionPane.showMessageDialog(null, "Successfully Reserved", "", JOptionPane.PLAIN_MESSAGE, imageIconObject.getSuccessImageIcon());
////                loadSavedDru
//            cacheTreatments();
//            reservationList.clear();
//            loadCashedReservedTable();
//            setButtonEnable(btn_save, false);
//            setButtonEnable(btn_give_treatment, false);
//            setButtonEnable(btn_reserve, false);
//            
//            setButtonEnable(btn_save, false);
//            setButtonEnable(btn_reserve, false);
//            setButtonEnable(btn_give_treatment, false);
//        } else {
//            JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
//        }

    }//GEN-LAST:event_btn_give_treatmentActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if (clinicId != null && (!clinicId.isEmpty())) {
            if (txt_mab_number.getText() != null && (!txt_mab_number.getText().toString().isEmpty())) {
                loadCacheTreatments();
                loadCashedReservedTable();
            } else {
            JOptionPane.showMessageDialog(null, "Please enter valid MAB number before proceed this action", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(null, "Please search patient by Clinic ID before proceed this action", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void drpd_drugListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_drpd_drugListActionPerformed
        // TODO add your handling code here:
        JComboBox comboBox = (JComboBox) evt.getSource();
if(comboBox.getSelectedItem() != null){
String selected = comboBox.getSelectedItem().toString();
            String[] values = selected.split("-");
            if (!values[0].contains("Item")) {
                List<Drug> selectedDrug = drugList
                        .stream()
                        .filter(c -> c.getDrugId().equals(values[0]))
                        .collect(Collectors.toList());
                setDrugStockLabel(jLabel1, selectedDrug.get(0).getVialCount());
                Drug.checkDrugAvailability(selectedDrug.get(0).getDrugId(), this.drugList, jLabel1);
            }
        }
                
    }//GEN-LAST:event_drpd_drugListActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MedicalTreatments.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MedicalTreatments.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MedicalTreatments.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MedicalTreatments.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new MedicalTreatments().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(MedicalTreatments.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_give_treatment;
    private javax.swing.JButton btn_update;
    private javax.swing.JComboBox<String> drpd_drugList;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbl_clinic_number;
    private javax.swing.JLabel lbl_error_patient_data_load_by_clinic_id;
    private javax.swing.JLabel lbl_id_number;
    private javax.swing.JLabel lbl_last_treatment;
    private javax.swing.JLabel lbl_last_treatment_date;
    private javax.swing.JLabel lbl_name;
    private javax.swing.JLabel lbl_ok_patient_data_load_by_clinic_id;
    private javax.swing.JTable reservedTable;
    private javax.swing.JTextField txt_clinic_id;
    private javax.swing.JTextField txt_mab_number;
    // End of variables declaration//GEN-END:variables
}
