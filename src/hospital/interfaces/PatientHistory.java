/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.interfaces;

import com.toedter.calendar.JDateChooser;
import common.CommonUtils;
import common.CrudOperations;
import constants.PatientTable;
import constants.TabelNames;
import database.DbConnection;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import models.Patient;
import models.PatientTreatmentHistory;

/**
 *
 * @author dilupa
 */
public class PatientHistory extends javax.swing.JFrame {

    Connection conn = null;
    String patient_id = "";
    String tableStatus = "SINGLE_MAB_1";
    int mabId = 0;
    int groupId = 0;
    int currentScopeCount = 0;
    String lastTreatmentDate;
    String error = "";
    ArrayList<PatientTreatmentHistory> patientHistoryList = new ArrayList<>();

    /**
     * Creates new form PatientHistory
     */
    public PatientHistory() throws SQLException {
        initComponents();
        DbConnection dbConnection = DbConnection.getInstance();
        conn = dbConnection.getConnection();
        //need to load all patient data here
        
//        loadSinglePatientHistory(true);
//        getLastTreatmentDate();
        setDefaultSinglePatientTable();
        lbl_ok_patient_data_load_by_clinic_id.setVisible(false);
        lbl_error_patient_data_load_by_clinic_id.setVisible(false);
        lbl_error_patient_data_not_load_by_clinic_id.setVisible(false);
        setDateSelectable(true);
        setClanderBackgroundColor(jDateChooser1, Color.GREEN);
        setClanderBackgroundColor(jDateChooser2, Color.GREEN);
        btn_expand_details.setVisible(false);
        btn_back.setVisible(false);
    }
    

    /**
     * Creates new form PatientHistory
     */
    public PatientHistory(String id) throws SQLException {
        patient_id = id;
        initComponents();
        DbConnection dbConnection = DbConnection.getInstance();
        conn = dbConnection.getConnection();
        loadSinglePatientHistory(true);
        getLastTreatmentDate();
        setAllMessageIconsFalse();
        setDateSelectable(true);
        btn_expand_details.setVisible(false);
        btn_back.setVisible(false);
    }

    public void setDateSelectable(Boolean status) {

        jDateChooser1.setEnabled(status);
        jDateChooser2.setEnabled(status);
    }
    
    public void setAllMessageIconsFalse() {
        lbl_ok_patient_data_load_by_clinic_id.setVisible(false);
        lbl_error_patient_data_load_by_clinic_id.setVisible(false);
        lbl_error_patient_data_not_load_by_clinic_id.setVisible(false);
    }
    public void setPatientClinicId(String clinicId) {
        this.patient_id = clinicId;
    }
    public void setDefaultSinglePatientGroupDetailsTable() {
//        tbl_history_table.setModel(null);
        tbl_history_table.setModel(new DefaultTableModel(new Object[][]{},
                new String[]{
                    "Drug Name", "Clinic ID", "Patient Name", "Contact", "Date"
                }));
        DefaultTableModel model = (DefaultTableModel) tbl_history_table.getModel();

        for (int i = 0; i < patientHistoryList.size(); i++) {
            model.addRow(new Object[]{
                patientHistoryList.get(i).getDrug_name(),
                patientHistoryList.get(i).getClinic_id(),
                patientHistoryList.get(i).getPatient_name(),
                patientHistoryList.get(i).getPatient_contact_number(),
//                patientHistoryList.get(i).getPatient_diagnosis(),
                patientHistoryList.get(i).getCreated_date()});

        }
    }
    public void setDefaultSinglePatientGroupCountTable() {
//        tbl_history_table.setModel(null);
        tbl_history_table.setModel(new DefaultTableModel(new Object[][]{},
                new String[]{
                    "Day", "Drug Count", "Clinic ID", "Patient Name", "Contact"
                }));
        DefaultTableModel model = (DefaultTableModel) tbl_history_table.getModel();

        for (int i = 0; i < patientHistoryList.size(); i++) {
            model.addRow(new Object[]{
                patientHistoryList.get(i).getGroup_number(),
                patientHistoryList.get(i).getGroup_count(),
                patientHistoryList.get(i).getClinic_id(),
                patientHistoryList.get(i).getPatient_name(),
                patientHistoryList.get(i).getPatient_contact_number(),
//                patientHistoryList.get(i).getPatient_diagnosis()
            });

        }
    }
    public void setDefaultSinglePatientTable() {
        tbl_history_table.setModel(new DefaultTableModel(new Object[][]{},
                new String[]{
                    "MAB Number","Teatment Days","Clinic ID", "Patient Name", "Contact"
                }));
        DefaultTableModel model = (DefaultTableModel) tbl_history_table.getModel();

        for (int i = 0; i < patientHistoryList.size(); i++) {
            model.addRow(new Object[]{
                patientHistoryList.get(i).getMab_number(),
                patientHistoryList.get(i).getMab_count(),
                patientHistoryList.get(i).getClinic_id(),
                patientHistoryList.get(i).getPatient_name(),
                patientHistoryList.get(i).getPatient_contact_number(),
//                patientHistoryList.get(i).getPatient_diagnosis()
            });

        }
    }
    public void setClanderBackgroundColor(JDateChooser dateChooser, Color color){
    dateChooser.setBackground(color);
    }
   public void loadCurrentPatientData(Boolean showResponse) {
        try {
            ResultSet rst = Patient.getOnePatientById(conn, patient_id);
            HashMap<String, Object> tableFieldsMap = new HashMap<>();

            tableFieldsMap = CrudOperations.getMapFromResultSet(rst, TabelNames.PATIENT);
            if (tableFieldsMap.size() > 0) {
                lbl_patient_name.setText(tableFieldsMap.get(PatientTable.NAME).toString());
                lbl_patient_clinic_number.setText(tableFieldsMap.get(PatientTable.CLINIC_ID).toString());
                lbl_patient_id.setText(tableFieldsMap.get(PatientTable.ID_NUMBER).toString());
//                lbl_ .setText(tableFieldsMap.get(PatientTable.CLINIC_ID).toString());
//                lbl_clinic_number.setText(tableFieldsMap.get(PatientTable.CLINIC_ID).toString());
//                lbl_clinic_number.setText(tableFieldsMap.get(PatientTable.CLINIC_ID).toString());
                System.out.println(tableFieldsMap);

                showHideLables(lbl_ok_patient_data_load_by_clinic_id);
            } else {
                showHideLables(lbl_error_patient_data_load_by_clinic_id);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void showHideLables(JLabel labelName) throws InterruptedException {
        labelName.setVisible(true);
//        Thread.currentThread().sleep(4000);
//        labelName.setVisible(false);
        int delay = 5000; //milliseconds
        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                labelName.setVisible(false);
            }
        };
        new javax.swing.Timer(delay, taskPerformer).start();
    }
    public void getLastTreatmentDate() {
        String sql = "SELECT MAX(dr.CREATED_ON_DATE) as CREATED_ON_DATE FROM drug_reservation dr, patient p where dr.PATIENT_ID='" + patient_id + "' and dr.PATIENT_ID = p.CLINIC_ID and dr.STATUS = 'COMPLETED' "+getDateQuary()+" ORDER BY dr.GROUP_NUMBER;";
        try {
            ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);

            while (rst.next()) {
                lastTreatmentDate = rst.getString("CREATED_ON_DATE");
                lastTreatmentDate = formatDate(new java.util.Date(lastTreatmentDate));
                jLabel12.setText(lastTreatmentDate);
            }
        } catch (Exception e) {
        }
    }
    public String getDateQuary(){
        if (jDateChooser2.getDate() == null && jDateChooser1.getDate() == null) {
            return "";
        } else if (jDateChooser2.getDate() == null) {
            error = "Please enter valid start date";
            return "";
        } else if (jDateChooser1.getDate() == null) {
            error = "Please enter valid end date";
            return "";
        } else {
            java.util.Date startTypeDate = jDateChooser1.getDate();
            java.util.Date endTypeDate = jDateChooser2.getDate();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String start = formatter.format(startTypeDate);
            String end = formatter.format(endTypeDate);
            String sql = " and dr.CREATED_ON_DATE between CAST('" + start + "' AS DATE) AND CAST('" + end + "' AS DATE)";
            return sql;
        }
    }
    public String formatDate(java.util.Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }
    public void setPatientDetails(){
        boolean isEmpty = true;
        try {
          ResultSet rst =   Patient.getOnePatientById(conn, patient_id);
          while (rst.next()) {
                isEmpty = false;
                lbl_patient_name.setText(rst.getString("NAME"));
                lbl_patient_clinic_number.setText(rst.getString("CLINIC_ID"));
                lbl_patient_id.setText(rst.getString("ID_NUMBER"));
            }
          if(isEmpty){
          
          }
        } catch (Exception e) {
        }
    }
    public void loadSinglePatientHistory(Boolean status) {
        boolean isEmpty = true;
//        jButton1.setVisible(false);
        btn_expand_details.setVisible(false);
        btn_back.setVisible(false);
        setDateSelectable(true);
        jLabel17.setText("MAB Details");
        String sql = "SELECT *,COUNT(DISTINCT GROUP_NUMBER) as GROUP_COUNT FROM drug_reservation dr, patient p , drugs d where dr.PATIENT_ID='" + this.patient_id + "' and dr.PATIENT_ID = p.CLINIC_ID and dr.STATUS = 'COMPLETED' and d.DRUG_ID = dr.DRUG_ID "+getDateQuary()+" group by dr.MAB_NUMBER;";
        String sqlForScopeCount = "SELECT COUNT(*) as SCOPE_COUNT FROM (SELECT dr.MAB_NUMBER FROM drug_reservation dr, patient p , drugs d where dr.PATIENT_ID='" + this.patient_id + "' and dr.PATIENT_ID = p.CLINIC_ID and dr.STATUS = 'COMPLETED' and d.DRUG_ID = dr.DRUG_ID "+getDateQuary()+" group by dr.MAB_NUMBER) AS SUBQUERY";
                                  
        ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);
        ResultSet rstScopeCount = CrudOperations.executeCustomQuary(conn, sqlForScopeCount);

        HashMap<String, String> patientHisData = new HashMap<>();
        try {
            while (rstScopeCount.next()) {
                currentScopeCount = rstScopeCount.getInt("SCOPE_COUNT");
                if (mabId != 0 && tableStatus != "SINGLE_MAB_EXPAND_2") {
                    jLabel17.setText("MAB " + mabId + " Details - Total : " + currentScopeCount);
                }else{
                jLabel17.setText("MAB Details - Total : " + currentScopeCount);
                }
            }
        } catch (Exception e) {
        }
        try {
            patientHistoryList.clear();
            
            while (rst.next()) {
                isEmpty = false;
                PatientTreatmentHistory history = new PatientTreatmentHistory();
                history.setMab_number(rst.getInt("MAB_NUMBER"));
                history.setMab_count(rst.getInt("GROUP_COUNT"));
                history.setClinic_id(rst.getString("CLINIC_ID"));
                history.setPatient_name(rst.getString("NAME"));
                history.setPatient_contact_number(rst.getString("CONTACT_NUMBER"));
                history.setPatient_diagnosis(rst.getString("DIAGNOSIS"));
                patientHistoryList.add(history);
                
                lbl_patient_name.setText(rst.getString("NAME"));
                lbl_patient_clinic_number.setText(rst.getString("CLINIC_ID"));
                lbl_patient_id.setText(rst.getString("ID_NUMBER"));
            }
            if (isEmpty == true) {
                try {
                    if (status) {
                        setPatientDetails();
                        setAllMessageIconsFalse();
                        showHideLables(lbl_error_patient_data_not_load_by_clinic_id);
                    }
                    JOptionPane.showMessageDialog(null, "No history records found!", "Error", JOptionPane.ERROR_MESSAGE);
                } catch (InterruptedException ex) {
                    setAllMessageIconsFalse();
                    showHideLables(lbl_error_patient_data_load_by_clinic_id);
                    Logger.getLogger(PatientHistory.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "Something went wrong!", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                if (status) {
                    setAllMessageIconsFalse();
                    showHideLables(lbl_ok_patient_data_load_by_clinic_id);
                }
            }
            setDefaultSinglePatientTable();
        } catch (Exception e) {
            System.out.println(e);
            if(status){
                try {
                    setAllMessageIconsFalse();
                    showHideLables(lbl_error_patient_data_load_by_clinic_id);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PatientHistory.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }
    public void loadSinglePatientHistoryByMabId(int mabId) {
        String sql = "SELECT *, COUNT(dr.GROUP_NUMBER) as GROUP_COUNT  FROM drug_reservation dr, patient p , drugs d where dr.PATIENT_ID='"+patient_id+"' and dr.PATIENT_ID = p.CLINIC_ID and dr.STATUS = 'COMPLETED' and d.DRUG_ID = dr.DRUG_ID and dr.MAB_NUMBER ="+this.mabId+getDateQuary()+" group by dr.GROUP_NUMBER ORDER BY dr.GROUP_NUMBER;";
        String sqlScopeCount = "SELECT COUNT(*) as SCOPE_COUNT FROM (SELECT dr.GROUP_NUMBER  FROM drug_reservation dr, patient p , drugs d where dr.PATIENT_ID='"+patient_id+"' and dr.PATIENT_ID = p.CLINIC_ID and dr.STATUS = 'COMPLETED' and d.DRUG_ID = dr.DRUG_ID and dr.MAB_NUMBER ="+this.mabId+getDateQuary()+" group by dr.GROUP_NUMBER ORDER BY dr.GROUP_NUMBER) AS SUBQUERY";
        ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);
        ResultSet rstScopeCount = CrudOperations.executeCustomQuary(conn, sqlScopeCount);

        HashMap<String, String> patientHisData = new HashMap<>();
                    
                    try {
             while (rstScopeCount.next()) {
                 currentScopeCount = rstScopeCount.getInt("SCOPE_COUNT");
                 jLabel17.setText("MAB "+mabId+" Treatment Days  - Total : "+currentScopeCount);
             }
        } catch (Exception e) {
        }
                    
        try {
            patientHistoryList.clear();
            while (rst.next()) {
                PatientTreatmentHistory history = new PatientTreatmentHistory();
//                history.setDrug_reservation_id(rst.getInt("DRUG_RESERVATION_ID"));
//                history.setDrug_id(rst.getString("DRUG_RESERVATION_ID"));
//                history.setDrug_name(rst.getString("DRUG_NAME"));
                history.setGroup_number(rst.getInt("GROUP_NUMBER"));
                history.setGroup_count(rst.getInt("GROUP_COUNT"));
                history.setClinic_id(rst.getString("CLINIC_ID"));
                history.setPatient_name(rst.getString("NAME"));
                history.setPatient_contact_number(rst.getString("CONTACT_NUMBER"));
                history.setPatient_diagnosis(rst.getString("DIAGNOSIS"));
//                history.setStatus(rst.getString("STATUS"));
//                history.setCreated_date(rst.getString("CREATED_ON_DATE"));
//                history.setPatient_id(rst.getString("PATIENT_ID"));

//                history.setId_number(rst.getString("ID_NUMBER"));

//                history.setPatient_address(rst.getString("ADDRESS"));
//                history.setPatient_contact_number(rst.getString("CONTACT_NUMBER"));
//                history.setPatient_weight(rst.getString("WEIGHT"));
//                history.setPatient_diagnosis(rst.getString("DIAGNOSIS"));
//                history.setPatient_gender(rst.getString("GENDER"));
//                history.setPatient_is_noted(rst.getString("IS_NOTED"));
                patientHistoryList.add(history);
            }
            setDefaultSinglePatientGroupCountTable();

        } catch (Exception e) {
            System.out.println(e);
        }

    }
    public void loadSinglePatientHistoryByMabIdAndGroupId() {
        String sql = "SELECT *, COUNT(dr.GROUP_NUMBER) as GROUP_COUNT FROM drug_reservation dr, patient p , drugs d where dr.PATIENT_ID='"+patient_id+"' and dr.PATIENT_ID = p.CLINIC_ID and dr.STATUS = 'COMPLETED' and d.DRUG_ID = dr.DRUG_ID and dr.MAB_NUMBER = "+mabId+getDateQuary()+" group by dr.GROUP_NUMBER;";
        ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);

        HashMap<String, String> patientHisData = new HashMap<>();

        try {
            patientHistoryList.clear();
            while (rst.next()) {
                PatientTreatmentHistory history = new PatientTreatmentHistory();
//                history.setDrug_reservation_id(rst.getInt("DRUG_RESERVATION_ID"));
//                history.setDrug_id(rst.getString("DRUG_RESERVATION_ID"));
//                history.setDrug_name(rst.getString("DRUG_NAME"));
                history.setGroup_number(rst.getInt("GROUP_NUMBER"));
                history.setGroup_count(rst.getInt("GROUP_COUNT"));
                history.setClinic_id(rst.getString("CLINIC_ID"));
                history.setPatient_name(rst.getString("NAME"));
                history.setPatient_contact_number(rst.getString("CONTACT_NUMBER"));
                history.setPatient_diagnosis(rst.getString("DIAGNOSIS"));
//                history.setStatus(rst.getString("STATUS"));
//                history.setCreated_date(rst.getString("CREATED_ON_DATE"));
//                history.setPatient_id(rst.getString("PATIENT_ID"));

//                history.setId_number(rst.getString("ID_NUMBER"));

//                history.setPatient_address(rst.getString("ADDRESS"));
//                history.setPatient_contact_number(rst.getString("CONTACT_NUMBER"));
//                history.setPatient_weight(rst.getString("WEIGHT"));
//                history.setPatient_diagnosis(rst.getString("DIAGNOSIS"));
//                history.setPatient_gender(rst.getString("GENDER"));
//                history.setPatient_is_noted(rst.getString("IS_NOTED"));
                patientHistoryList.add(history);
            }
            setDefaultSinglePatientGroupCountTable();

        } catch (Exception e) {
            System.out.println(e);
        }

    }
    public void loadSinglePatientHistoryByMabIdAndGroupIdWithDetails() {
        String sql = "SELECT * FROM drug_reservation dr, patient p , drugs d where dr.PATIENT_ID='"+patient_id+"' and dr.PATIENT_ID = p.CLINIC_ID and dr.STATUS = 'COMPLETED' and d.DRUG_ID = dr.DRUG_ID and dr.MAB_NUMBER = "+mabId+" and dr.GROUP_NUMBER="+groupId+getDateQuary()+";";
        ResultSet rst = CrudOperations.executeCustomQuary(conn, sql);

        HashMap<String, String> patientHisData = new HashMap<>();

        try {
            patientHistoryList.clear();
            while (rst.next()) {
                PatientTreatmentHistory history = new PatientTreatmentHistory();
//                history.setDrug_reservation_id(rst.getInt("DRUG_RESERVATION_ID"));
//                history.setDrug_id(rst.getString("DRUG_RESERVATION_ID"));
                history.setDrug_name(rst.getString("DRUG_NAME"));
//                history.setGroup_number(rst.getInt("GROUP_NUMBER"));
//                history.setGroup_count(rst.getInt("GROUP_COUNT"));
                history.setClinic_id(rst.getString("CLINIC_ID"));
                history.setPatient_name(rst.getString("NAME"));
                history.setPatient_contact_number(rst.getString("CONTACT_NUMBER"));
                history.setPatient_diagnosis(rst.getString("DIAGNOSIS"));
//                history.setStatus(rst.getString("STATUS"));
                history.setCreated_date(rst.getString("CREATED_ON_DATE"));
//                history.setPatient_id(rst.getString("PATIENT_ID"));

//                history.setId_number(rst.getString("ID_NUMBER"));

//                history.setPatient_address(rst.getString("ADDRESS"));
//                history.setPatient_contact_number(rst.getString("CONTACT_NUMBER"));
//                history.setPatient_weight(rst.getString("WEIGHT"));
//                history.setPatient_diagnosis(rst.getString("DIAGNOSIS"));
//                history.setPatient_gender(rst.getString("GENDER"));
//                history.setPatient_is_noted(rst.getString("IS_NOTED"));
                patientHistoryList.add(history);
            }
            setDefaultSinglePatientGroupDetailsTable();

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_history_table = new javax.swing.JTable();
        btn_back = new javax.swing.JButton();
        txt_clinic_number = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        lbl_ok_patient_data_load_by_clinic_id = new javax.swing.JLabel();
        lbl_error_patient_data_load_by_clinic_id = new javax.swing.JLabel();
        lbl_error_patient_data_not_load_by_clinic_id = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btn_expand_details = new javax.swing.JButton();
        lbl_patient_name = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lbl_patient_clinic_number = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel8 = new javax.swing.JLabel();
        lbl_patient_id = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1000, 700));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(51, 0, 51));
        jPanel1.setToolTipText("");
        jPanel1.setLayout(null);

        tbl_history_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_history_table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_history_tableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_history_table);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(70, 340, 850, 290);

        btn_back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/undo.png"))); // NOI18N
        btn_back.setText("back");
        btn_back.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_back.setMargin(new java.awt.Insets(0, 1, 0, 0));
        btn_back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_backActionPerformed(evt);
            }
        });
        jPanel1.add(btn_back);
        btn_back.setBounds(590, 290, 100, 40);

        txt_clinic_number.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_clinic_numberActionPerformed(evt);
            }
        });
        jPanel1.add(txt_clinic_number);
        txt_clinic_number.setBounds(220, 120, 180, 28);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("To");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(690, 130, 60, 17);

        lbl_ok_patient_data_load_by_clinic_id.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/save.png"))); // NOI18N
        jPanel1.add(lbl_ok_patient_data_load_by_clinic_id);
        lbl_ok_patient_data_load_by_clinic_id.setBounds(560, 190, 40, 40);

        lbl_error_patient_data_load_by_clinic_id.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/warning.png"))); // NOI18N
        jPanel1.add(lbl_error_patient_data_load_by_clinic_id);
        lbl_error_patient_data_load_by_clinic_id.setBounds(560, 190, 40, 40);

        lbl_error_patient_data_not_load_by_clinic_id.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/not-found.png"))); // NOI18N
        jPanel1.add(lbl_error_patient_data_not_load_by_clinic_id);
        lbl_error_patient_data_not_load_by_clinic_id.setBounds(560, 190, 40, 40);

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jLabel17);
        jLabel17.setBounds(260, 300, 370, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Name");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(100, 180, 70, 17);

        btn_expand_details.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/expand.png"))); // NOI18N
        btn_expand_details.setText("Expand");
        btn_expand_details.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_expand_details.setMargin(new java.awt.Insets(0, 1, 0, 0));
        btn_expand_details.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_expand_detailsActionPerformed(evt);
            }
        });
        jPanel1.add(btn_expand_details);
        btn_expand_details.setBounds(700, 290, 220, 40);

        lbl_patient_name.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_patient_name.setForeground(new java.awt.Color(255, 255, 255));
        lbl_patient_name.setText("--------------");
        jPanel1.add(lbl_patient_name);
        lbl_patient_name.setBounds(220, 180, 180, 20);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Clinic Number");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(100, 210, 100, 17);

        lbl_patient_clinic_number.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_patient_clinic_number.setForeground(new java.awt.Color(255, 255, 255));
        lbl_patient_clinic_number.setText("--------------");
        jPanel1.add(lbl_patient_clinic_number);
        lbl_patient_clinic_number.setBounds(220, 210, 180, 17);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("ID Number");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(100, 240, 80, 17);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Clinic Number");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(100, 130, 100, 17);

        jDateChooser2.setBackground(new java.awt.Color(204, 255, 204));
        jPanel1.add(jDateChooser2);
        jDateChooser2.setBounds(720, 120, 170, 28);

        jDateChooser1.setBackground(new java.awt.Color(204, 255, 204));
        jPanel1.add(jDateChooser1);
        jDateChooser1.setBounds(510, 120, 160, 28);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("From");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(460, 130, 60, 17);

        lbl_patient_id.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_patient_id.setForeground(new java.awt.Color(255, 255, 255));
        lbl_patient_id.setText("--------------");
        jPanel1.add(lbl_patient_id);
        lbl_patient_id.setBounds(220, 240, 190, 17);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Last Treatment Date");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(630, 180, 150, 17);

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("--------------");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(800, 180, 110, 17);

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Last Treament");
        jPanel1.add(jLabel13);
        jLabel13.setBounds(640, 210, 100, 17);

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("--------------");
        jPanel1.add(jLabel14);
        jLabel14.setBounds(800, 210, 120, 17);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/removed.png"))); // NOI18N
        jButton1.setText("Remove");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(70, 290, 120, 40);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/people.png"))); // NOI18N
        jButton2.setText("All Patients");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(70, 40, 150, 50);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/home.png"))); // NOI18N
        jButton3.setText("Home");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(650, 40, 110, 50);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/patient_injection_icon.png"))); // NOI18N
        jButton4.setText("Treatments");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);
        jButton4.setBounds(780, 40, 140, 50);

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Patient History");
        jPanel1.add(jLabel15);
        jLabel15.setBounds(320, 30, 280, 50);

        jLabel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 10, 960, 640);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/page_icons/search_label.png"))); // NOI18N
        jButton5.setText("Search");
        jButton5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton5MouseClicked(evt);
            }
        });
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton5);
        jButton5.setBounds(410, 190, 120, 40);

        jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));
        jPanel1.add(jLabel5);
        jLabel5.setBounds(70, 100, 850, 170);

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/walpapers/home.jpg"))); // NOI18N
        jLabel16.setText("jLabel1");
        jLabel16.setMaximumSize(new java.awt.Dimension(900, 600));
        jLabel16.setMinimumSize(new java.awt.Dimension(900, 600));
        jLabel16.setPreferredSize(new java.awt.Dimension(900, 600));
        jPanel1.add(jLabel16);
        jLabel16.setBounds(0, 0, 1000, 800);

        jLabel1.setText("jLabel1");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(560, 310, 34, 14);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1000, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1000, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 800, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 800, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_clinic_numberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_clinic_numberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_clinic_numberActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        Home home;
        try {
            home = new Home();
            home.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(MedicalTreatments.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        AllPatients allPatients;
        try {
            allPatients = new AllPatients();
            allPatients.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        try {
            MedicalTreatments medicalTreatments = new MedicalTreatments(patient_id);
            medicalTreatments.setPatientClinicId(patient_id);
            medicalTreatments.setVisible(true);
            medicalTreatments.loadCurrentPatientData(false);
            this.dispose();
        } catch (Exception e) {
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void tbl_history_tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_history_tableMouseClicked
        // TODO add your handling code here:
        int row = tbl_history_table.getSelectedRow();
        if (tableStatus == "SINGLE_MAB_1") {
            String mab = tbl_history_table.getValueAt(row, 0).toString();
            mabId = Integer.parseInt(mab);
            btn_expand_details.setText("Expand MAB "+mab+" details");
        }
        if (tableStatus == "SINGLE_GROUP_3") {
            btn_expand_details.setVisible(false);
        } else {
            btn_expand_details.setVisible(true);
        }
        String group = tbl_history_table.getValueAt(row, 0).toString();
        if (tableStatus != "SINGLE_GROUP_3") {
            groupId = Integer.parseInt(group);
            if (tableStatus != "SINGLE_MAB_1") {
             btn_expand_details.setText("Expand MAB "+mabId+" Day "+groupId+" details");
            }
        }


    }//GEN-LAST:event_tbl_history_tableMouseClicked

    private void btn_expand_detailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_expand_detailsActionPerformed
        // TODO add your handling code here:
        Boolean status = false;
        btn_expand_details.setVisible(false);
        if (tableStatus == "SINGLE_MAB_1" && status== false) {
            tableStatus = "SINGLE_MAB_EXPAND_2";
                    jLabel17.setText("MAB "+mabId+" Treatment Days");
            status = true;
            loadSinglePatientHistoryByMabId(mabId);
        }
//        if (tableStatus == "SINGLE_MAB_EXPAND_2" && status== false) {
//            tableStatus = "SINGLE_GROUP_3";
//            status = true;
//            loadSinglePatientHistoryByMabIdAndGroupId();
//        }
        if (tableStatus == "SINGLE_MAB_EXPAND_2" && status== false) {
            tableStatus = "SINGLE_GROUP_3";
            btn_expand_details.setVisible(false);
            jLabel17.setText("MAB "+mabId+ " Day "+groupId+" Treatment Details");
            status = true;
            loadSinglePatientHistoryByMabIdAndGroupIdWithDetails();
        }
        btn_back.setVisible(true);
        setDateSelectable(false);
        
    }//GEN-LAST:event_btn_expand_detailsActionPerformed

    private void btn_backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_backActionPerformed
        // TODO add your handling code here:
        Boolean status = false;
        if (tableStatus == "SINGLE_GROUP_3" && status== false) {
            status = true;
            jLabel17.setText("MAB " + mabId + " Treatment Days");
            loadSinglePatientHistoryByMabId(mabId);
            tableStatus = "SINGLE_MAB_EXPAND_2";
            
        }
        if(tableStatus == "SINGLE_MAB_EXPAND_2" && status== false){
            status = true;
            loadSinglePatientHistory(false);
            tableStatus = "SINGLE_MAB_1";
            btn_back.setVisible(false);
            setDateSelectable(true);
        }
    }//GEN-LAST:event_btn_backActionPerformed

    private void jButton5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton5MouseClicked
        // TODO add your handling code here:
//        432423CLINICID123
        patient_id = txt_clinic_number.getText().toString();
        loadSinglePatientHistory(true);

    }//GEN-LAST:event_jButton5MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
//        CommonUtils.compairTwoDates
        patient_id = txt_clinic_number.getText().toString();
        if (!patient_id.isEmpty()) {
            java.util.Date startTypeDate = jDateChooser1.getDate();
            java.util.Date endTypeDate = jDateChooser2.getDate();
            if (!CommonUtils.checkDate(startTypeDate) && !CommonUtils.checkDate(endTypeDate)) {
                loadSinglePatientHistory(true);
                getLastTreatmentDate();
                setClanderBackgroundColor(jDateChooser1, Color.GREEN);
                setClanderBackgroundColor(jDateChooser2, Color.GREEN);
            } else if (!CommonUtils.checkDate(startTypeDate)) {
                setClanderBackgroundColor(jDateChooser1, Color.RED);
                JOptionPane.showMessageDialog(null, "Please select valid start date", "Error", JOptionPane.ERROR_MESSAGE);
            } else if (!CommonUtils.checkDate(endTypeDate)) {
                setClanderBackgroundColor(jDateChooser2, Color.RED);
                JOptionPane.showMessageDialog(null, "Please select valid end date", "Error", JOptionPane.ERROR_MESSAGE);
            } else if (!(CommonUtils.compairTwoDates(startTypeDate, endTypeDate))) {
                setClanderBackgroundColor(jDateChooser1, Color.RED);
                setClanderBackgroundColor(jDateChooser2, Color.RED);
                JOptionPane.showMessageDialog(null, "Please select valid date range", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                setClanderBackgroundColor(jDateChooser1, Color.GREEN);
                setClanderBackgroundColor(jDateChooser2, Color.GREEN);
                loadSinglePatientHistory(true);
                getLastTreatmentDate();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please enter patient Clinic ID", "Error", JOptionPane.ERROR_MESSAGE);
        }
        
        
        
    }//GEN-LAST:event_jButton5ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PatientHistory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PatientHistory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PatientHistory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PatientHistory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new PatientHistory().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(PatientHistory.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_back;
    private javax.swing.JButton btn_expand_details;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_error_patient_data_load_by_clinic_id;
    private javax.swing.JLabel lbl_error_patient_data_not_load_by_clinic_id;
    private javax.swing.JLabel lbl_ok_patient_data_load_by_clinic_id;
    private javax.swing.JLabel lbl_patient_clinic_number;
    private javax.swing.JLabel lbl_patient_id;
    private javax.swing.JLabel lbl_patient_name;
    private javax.swing.JTable tbl_history_table;
    private javax.swing.JTextField txt_clinic_number;
    // End of variables declaration//GEN-END:variables
}
