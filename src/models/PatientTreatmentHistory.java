/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author dilupa
 */
public class PatientTreatmentHistory {
    int drug_reservation_id;
    String drug_id;
    String drug_name;
    int mab_number;
    int mab_count;
    int group_number;
    int group_count;
    String status;
    String created_date;
    String patient_id;
    String clinic_id;
    String id_number;
    String patient_name;
    String patient_address;
    String patient_contact_number;
    String patient_weight;
    String patient_diagnosis;
    String patient_gender;
    String patient_is_noted;

    public int getDrug_reservation_id() {
        return drug_reservation_id;
    }

    public void setDrug_reservation_id(int drug_reservation_id) {
        this.drug_reservation_id = drug_reservation_id;
    }

    public String getDrug_id() {
        return drug_id;
    }

    public void setDrug_id(String drug_id) {
        this.drug_id = drug_id;
    }

    public String getDrug_name() {
        return drug_name;
    }

    public void setDrug_name(String drug_name) {
        this.drug_name = drug_name;
    }

    public int getMab_number() {
        return mab_number;
    }

    public void setMab_number(int mab_number) {
        this.mab_number = mab_number;
    }

    public int getMab_count() {
        return mab_count;
    }

    public void setMab_count(int mab_count) {
        this.mab_count = mab_count;
    }

    public int getGroup_number() {
        return group_number;
    }

    public void setGroup_number(int group_number) {
        this.group_number = group_number;
    }

    public int getGroup_count() {
        return group_count;
    }

    public void setGroup_count(int group_count) {
        this.group_count = group_count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getClinic_id() {
        return clinic_id;
    }

    public void setClinic_id(String clinic_id) {
        this.clinic_id = clinic_id;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public String getPatient_address() {
        return patient_address;
    }

    public void setPatient_address(String patient_address) {
        this.patient_address = patient_address;
    }

    public String getPatient_contact_number() {
        return patient_contact_number;
    }

    public void setPatient_contact_number(String patient_contact_number) {
        this.patient_contact_number = patient_contact_number;
    }

    public String getPatient_weight() {
        return patient_weight;
    }

    public void setPatient_weight(String patient_weight) {
        this.patient_weight = patient_weight;
    }

    public String getPatient_diagnosis() {
        return patient_diagnosis;
    }

    public void setPatient_diagnosis(String patient_diagnosis) {
        this.patient_diagnosis = patient_diagnosis;
    }

    public String getPatient_gender() {
        return patient_gender;
    }

    public void setPatient_gender(String patient_gender) {
        this.patient_gender = patient_gender;
    }

    public String getPatient_is_noted() {
        return patient_is_noted;
    }

    public void setPatient_is_noted(String patient_is_noted) {
        this.patient_is_noted = patient_is_noted;
    }
    
    
}
