/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import common.CommonUtils;
import common.CrudOperations;
import constants.DrugReservationTable;
import constants.DrugsTable;
import constants.TabelNames;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author dilupa
 */
public class DrugReservation {
    int mab_number;
    int group_number;
    int reservation_id;
    String drug;
    LocalDateTime date;
    String status;
    String patient_id;
    String drug_id;
    
    public int getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(int reservation_id) {
        this.reservation_id = reservation_id;
    }

    public String getDrug_id() {
        return drug_id;
    }

    public void setDrug_id(String drug_id) {
        this.drug_id = drug_id;
    }
     public int getGroup_number() {
        return group_number;
    }

    public void setGroup_number(int group_number) {
        this.group_number = group_number;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
    public DrugReservation(){
    }
    public DrugReservation(int mab_number, String drug, String drug_id, LocalDateTime date, String status, String patient_id, int reservation_id, int group_number) {
        this.mab_number = mab_number;
        this.group_number = group_number;
        this.drug_id = drug_id;
        this.drug = drug;
        this.date = date;
        this.status = status;
        this.patient_id = patient_id;
        this.reservation_id = reservation_id;
    }

    public Boolean insertDrugReservation(Connection connection) {
        HashMap<String, Object> drugData = new HashMap<>();
        drugData.put(DrugReservationTable.DRUG_ID, this.getDrug_id());
        drugData.put(DrugReservationTable.MAB_NUMBER, this.getMabNumber());
        drugData.put(DrugReservationTable.GROUP_NUMBER, this.getGroup_number());
        drugData.put(DrugReservationTable.STATUS, this.getStatus());
        drugData.put(DrugReservationTable.CREATED_ON, this.getDate());
        return CrudOperations.InsertOne(connection, drugData,
                TabelNames.DRUG_RESERVATION);
    }
    public Boolean updateBatchDrugReservation(Connection connection,
            ArrayList<DrugReservation> drugReservationList) {
        try {
            String sql = CommonUtils.generateSqlForBatchUpdate(TabelNames.DRUG_RESERVATION);
                PreparedStatement pstmt = connection.prepareStatement(sql);
            for (DrugReservation reservation : drugReservationList) {

                pstmt.setString(1, reservation.getDrug_id());
                pstmt.setInt(2, reservation.getMabNumber());
                pstmt.setInt(3, reservation.getGroup_number());
                pstmt.setString(4,reservation.getStatus());
                pstmt.setString(5, reservation.getDate().toString());
                pstmt.setString(6, reservation.getPatient_id());
                pstmt.setInt(7, reservation.getReservation_id());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            return true;
        } catch (Exception e) {
        return false;
        }
        
    }
    public Boolean deleteBatchDrugReservation(Connection connection,
            ArrayList<DrugReservation> drugReservationList) {
        try {
            String sql = CommonUtils.generateSqlForDelete(TabelNames.DRUG_RESERVATION,"DRUG_RESERVATION_ID");
            PreparedStatement pstmt = connection.prepareStatement(sql);
            for (DrugReservation reservation : drugReservationList) {
                pstmt.setInt(1, reservation.getReservation_id());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            return true;
        } catch (Exception e) {
            return false;
        }

//        return CrudOperations.InsertBatch(connection, drugReservationList, TabelNames.DRUG_RESERVATION, drugReservationFields);
    }
    public Boolean insertBatchDrugReservation(Connection connection,
            ArrayList<DrugReservation> drugReservationList, String status) {
        try {
            String sql = CommonUtils.generateSqlForBatchInsert(TabelNames.DRUG_RESERVATION);
            PreparedStatement pstmt = connection.prepareStatement(sql);
            for (DrugReservation reservation : drugReservationList) {
                pstmt.setInt(1, 0);
                pstmt.setString(2, reservation.getDrug_id());
                pstmt.setInt(3, reservation.getMabNumber());
                pstmt.setString(4, status);
                pstmt.setString(5, reservation.getDate().toString());
                pstmt.setString(6, reservation.getPatient_id());
                pstmt.setInt(7, reservation.getGroup_number());



                pstmt.addBatch();
            }
            pstmt.executeBatch();
            return true;
        } catch (Exception e) {
            return false;
        }

//        return CrudOperations.InsertBatch(connection, drugReservationList, TabelNames.DRUG_RESERVATION, drugReservationFields);
    }
    public int getMabNumber() {
        return mab_number;
    }

    public void setMabNumber(int mab_number) {
        this.mab_number = mab_number;
    }

    public String getDrug() {
        return drug;
    }

    public void setDrug(String drug) {
        this.drug = drug;
    }
}
