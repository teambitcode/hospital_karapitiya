/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import common.CrudOperations;
import constants.DrugsTable;
import constants.TabelNames;
import java.awt.Color;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.JLabel;

/**
 *
 * @author dilupa
 */
public class Drug {
    String drugId;
    String drugName;
    int vialCount;
    String createdOn;
    String updatedOn;

    @Override
    public String toString() {
        return this.drugName;
    }
    
    public String getDrugId() {
        return drugId;
    }

    public void setDrugId(String drugId) {
        this.drugId = drugId;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public Integer getVialCount() {
        return vialCount;
    }

    public void setVialCount(Integer vialCount) {
        this.vialCount = vialCount;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public static ArrayList<Drug> drugsAsArrayList(Connection connection) {
        ArrayList<Drug> drugList = new ArrayList<>();
        ResultSet rst = getAllDrugsAsResultSetForTable(connection);

        try {
            while (rst.next()) {
                Drug drug = new Drug();
                drug.setDrugId(rst.getString("DRUG_ID"));
                drug.setDrugName(rst.getString("DRUG_NAME"));
                drug.setVialCount(rst.getInt("VIAL_COUNT"));
//                                drug.setCreatedOn(rst.getString("CREATED_ON"));
//                                drug.setUpdatedOn(rst.getString("UPDATED_ON"));
                drugList.add(drug);
            }
        } catch (Exception e) {
        }
        return drugList;
    }
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
    public static Boolean deleteDrugById(Connection connection, int id) {

        return CrudOperations.deleteItem(connection, TabelNames.DRUGS, id,DrugsTable.DRUG_ID,null);
    }
    public static ResultSet getAllDrugsAsResultSetForTable(Connection connection) {
        Set<String> setA = new HashSet<>();

        setA.add(DrugsTable.DRUG_ID);
        setA.add(DrugsTable.DRUG_NAME);
        setA.add(DrugsTable.VIAL_COUNT);
//        setA.add(DrugsTable.UPDATED_ON);

        return CrudOperations.SelectAll(connection, TabelNames.DRUGS, setA);
    }

    public static ResultSet getPatientsByCondition(Connection connection, String condition, String value) {
        return CrudOperations.getItemsDetailsByCondition(connection, TabelNames.DRUGS, condition, value);
    }
    public String getEmptyFields() {
        String errorMessage = "Below fields are empty: \n";
        HashMap<String, String> drugTableRedableColumnNamesMap = (HashMap<String, String>) DrugsTable.getReadableNamesMap();
        if (this.drugName == null) {
            errorMessage += drugTableRedableColumnNamesMap.get(DrugsTable.DRUG_NAME);
        }
        if (this.vialCount == 0) {
            errorMessage += drugTableRedableColumnNamesMap.get(DrugsTable.VIAL_COUNT);
        }
        return errorMessage;
    }

 public Boolean insertDrug(Connection connection) {
        HashMap<String, Object> drugData = new HashMap<>();
        drugData.put(DrugsTable.DRUG_NAME, this.getDrugName());
        drugData.put(DrugsTable.VIAL_COUNT, this.getVialCount());
        return CrudOperations.InsertOne(connection, drugData, TabelNames.DRUGS);
    }

    public Boolean updateDrug(Connection connection, Map<String, Object> updateData, Object id) {
        boolean status = false;
        try {
            status = CrudOperations.update(connection, TabelNames.DRUGS, id, updateData, DrugsTable.DRUG_ID);
        } catch (Exception e) {
        }
        return status;
    }
    
    public static void checkDrugAvailability(String id, ArrayList<Drug> drugList, JLabel drugLableStock) {
        for (int i = 0; i < drugList.size(); i++) {
            if (drugList.get(i).getDrugId().equals(id)) {
                if (drugList.get(i).getVialCount() <= 5) {
                    drugLableStock.setForeground(Color.RED);
                } else if (drugList.get(i).getVialCount() <= 10) {
                    drugLableStock.setForeground(Color.YELLOW);
                } else {
                    drugLableStock.setForeground(Color.GREEN);
                }
            }
        }
    }
}
