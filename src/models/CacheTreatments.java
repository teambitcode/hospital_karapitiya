/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author dilupa
 */
public class CacheTreatments {

    String patient_id;
    String drug_ids;

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getDrug_ids() {
        return drug_ids;
    }

    public void setDrug_ids(String drug_ids) {
        this.drug_ids = drug_ids;
    }
    
    
}
