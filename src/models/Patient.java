/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import common.CrudOperations;
import constants.PatientTable;
import constants.TabelNames;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author dilupa
 */
public class Patient {

    private String clinicId;
    private String idNumber;
    private String name;
    private String address;
    private String contactNumber;
    private String weight;
    private String diagnosis;
    private String gender;
    private String isNoted;

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIsNoted() {
        return isNoted;
    }
public void setIsNoted(String isNoted) {
        this.isNoted = isNoted;
    }
    public String getEmptyFields() {
        String errorMessage = "Below fields are empty: \n";
        HashMap<String, String> patientTableRedableColumnNamesMap = (HashMap<String, String>) PatientTable.getReadableNamesMap();
        if (this.clinicId == null) {
            errorMessage += patientTableRedableColumnNamesMap.get(PatientTable.CLINIC_ID);
        }
        if (this.idNumber == null) {
            errorMessage += patientTableRedableColumnNamesMap.get(PatientTable.ID_NUMBER);
        }
        if (this.name == null) {
            errorMessage += patientTableRedableColumnNamesMap.get(PatientTable.NAME);
        }
        if (this.contactNumber == null) {
            errorMessage += patientTableRedableColumnNamesMap.get(PatientTable.CONTACT_NUMBER);
        }
        if (this.weight == null) {
            errorMessage += patientTableRedableColumnNamesMap.get(PatientTable.WEIGHT);
        }
        if (this.gender == null) {
            errorMessage += patientTableRedableColumnNamesMap.get(PatientTable.GENDER);
        }
        if (this.isNoted == null) {
            errorMessage += patientTableRedableColumnNamesMap.get(PatientTable.IS_NOTED);
        }
        return errorMessage;
    }

    public ArrayList<Patient> getAllPatientsAsArrayList() {
        return null;
    }
    public static Boolean deletePatient(Connection connection, String tableName, String id, String idColumn) {
        boolean status = false;
        try {
            status = CrudOperations.deleteItem(connection, tableName, id, idColumn,null);
        } catch (Exception e) {
        }
        return status;
    }
    public Boolean updatePatient(Connection connection, Map<String, Object> updateData, String id) {
        boolean status = false;
        try {
            status = CrudOperations.update(connection, TabelNames.PATIENT, id, updateData, PatientTable.CLINIC_ID);
        } catch (Exception e) {
        }
        return status;
    }

    public static void clearAll(JTextField textField) {
        textField.setText("");
    }

    public static void clearAll(JTextArea textArea) {
        textArea.setText("");
    }

    public static void clearAll(JComboBox cmb) {
        cmb.setSelectedIndex(0);
    }
    public static ResultSet getOnePatientById(Connection connection, String id) {
        return CrudOperations.getItemDetailsById(connection, TabelNames.PATIENT, PatientTable.CLINIC_ID, id);
    }
    
    public static ResultSet getPatientsByCondition(Connection connection, String condition, String value) {
        return CrudOperations.getItemsDetailsByCondition(connection, TabelNames.PATIENT, condition, value);
    }

    public static ResultSet getAllPatientsAsResultSet(Connection connection) {
        return CrudOperations.SelectAll(connection, TabelNames.PATIENT);
    }

    public static ResultSet getAllPatientsAsResultSetForTable(Connection connection) {
        Set<String> setA = new HashSet<>();

        setA.add(PatientTable.CLINIC_ID);
        setA.add(PatientTable.ID_NUMBER);
        setA.add(PatientTable.NAME);
        setA.add(PatientTable.GENDER);

        return CrudOperations.SelectAll(connection, TabelNames.PATIENT, setA);
    }

    public Boolean insertPatient(Connection connection) {
        HashMap<String, Object> patientData = new HashMap<>();
        patientData.put(PatientTable.CLINIC_ID, this.getClinicId());
        patientData.put(PatientTable.ID_NUMBER, this.getIdNumber());
        patientData.put(PatientTable.NAME, this.getName());
        patientData.put(PatientTable.CONTACT_NUMBER, this.getContactNumber());
        patientData.put(PatientTable.ADDRESS, this.getAddress());
        patientData.put(PatientTable.WEIGHT, this.getWeight());
        patientData.put(PatientTable.DIAGNOSIS, this.getDiagnosis());
        patientData.put(PatientTable.GENDER, this.getGender());
        patientData.put(PatientTable.IS_NOTED, this.getIsNoted());
        return CrudOperations.InsertOne(connection, patientData, TabelNames.PATIENT);
    }

}
