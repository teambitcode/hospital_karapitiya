/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import common.CrudOperations;
import constants.DrugReservationPerPatientTable;
import constants.TabelNames;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import static models.Drug.getAllDrugsAsResultSetForTable;

/**
 *
 * @author dilupa
 */
public class DrugReservationPerPatientData {
    int id;
    int drug_id;
    String drug_name;
    String date;
    String clinic_id;
    int vial_count;

    public int geId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getDrug_name() {
        return drug_name;
    }

    public void setDrug_name(String drug_name) {
        this.drug_name = drug_name;
    }
    public int getDrug_id() {
        return drug_id;
    }

    public void setDrug_id(int drug_id) {
        this.drug_id = drug_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClinic_id() {
        return clinic_id;
    }

    public void setClinic_id(String clinic_id) {
        this.clinic_id = clinic_id;
    }

    public int getVial_count() {
        return vial_count;
    }

    public void setVial_count(int vial_count) {
        this.vial_count = vial_count;
    }

    public DrugReservationPerPatientData() {
    }

    public DrugReservationPerPatientData(int drug_id, String date, String clinic_id, int vial_count) {
        this.drug_id = drug_id;
        this.date = date;
        this.clinic_id = clinic_id;
        this.vial_count = vial_count;
    }
    
    public Boolean insertDrugReservationPerPatient(Connection connection) {
        HashMap<String, Object> drugReservationPerPatientData = new HashMap<>();
        drugReservationPerPatientData.put(DrugReservationPerPatientTable.DRUG_ID, this.getDrug_id());
        drugReservationPerPatientData.put(DrugReservationPerPatientTable.CLINIC_ID, this.getClinic_id());
        drugReservationPerPatientData.put(DrugReservationPerPatientTable.VIAL_COUNT, this.getVial_count());
        drugReservationPerPatientData.put(DrugReservationPerPatientTable.UPDATED_DATE, this.getDate());
        return CrudOperations.InsertOne(connection, drugReservationPerPatientData,
                TabelNames.DRUG_RESERVATION_PER_PATIENT);
    }
    public static Boolean updateDrug(Connection connection, Map<String, Object> updateData, Object id) {
        boolean status = false;
        try {
            status = CrudOperations.update(connection, TabelNames.DRUG_RESERVATION_PER_PATIENT, id, updateData, DrugReservationPerPatientTable.ID);
        } catch (Exception e) {
        }
        return status;
    }
     public static Boolean deleteDrugById(Connection connection, int id) {

        return CrudOperations.deleteItem(connection, TabelNames.DRUG_RESERVATION_PER_PATIENT, id,DrugReservationPerPatientTable.ID,null);
    }
     public static ResultSet getAllDrugsReservationPerPatientAsResultSetForTable(Connection connection) {
         Set<String> setA = new HashSet<>();

         setA.add(DrugReservationPerPatientTable.ID);
         setA.add(DrugReservationPerPatientTable.DRUG_ID);
         setA.add(DrugReservationPerPatientTable.CLINIC_ID);
         setA.add(DrugReservationPerPatientTable.VIAL_COUNT);
         setA.add(DrugReservationPerPatientTable.UPDATED_DATE);
//        setA.add(DrugsTable.UPDATED_ON);

        return CrudOperations.SelectAll(connection, TabelNames.DRUG_RESERVATION_PER_PATIENT, setA);
    }

    public static ResultSet getItemsByCondition(Connection connection, String condition, String value) {
        return CrudOperations.getItemsDetailsByCondition(connection, TabelNames.DRUG_RESERVATION_PER_PATIENT, condition, value);
    }
    
    public static ArrayList<Drug> drugsAsArrayList(Connection connection, String clinic_id) {
        ArrayList<Drug> drugList = new ArrayList<>();
        String sql = "SELECT *, drp.VIAL_COUNT as P_VIAL_COUNT FROM DRUGS d,DRUG_RESERVATION_PER_PATIENT drp WHERE drp.CLINIC_ID = '" + clinic_id + "' and d.DRUG_ID = drp.DRUG_ID";
        ResultSet rst = CrudOperations.executeCustomQuary(connection,sql);

        try {
            while (rst.next()) {
                Drug drug = new Drug();
                drug.setDrugId(rst.getString("DRUG_ID"));
                drug.setDrugName(rst.getString("DRUG_NAME"));
                drug.setVialCount(rst.getInt("P_VIAL_COUNT"));
//                                drug.setCreatedOn(rst.getString("CREATED_ON"));
//                                drug.setUpdatedOn(rst.getString("UPDATED_ON"));
                drugList.add(drug);
            }
        } catch (Exception e) {
        }
        return drugList;
    }
}
